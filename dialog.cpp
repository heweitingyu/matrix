#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    ui->row->addItem("1");
    ui->row->addItem("2");
    ui->row->addItem("3");
    ui->row->addItem("4");
    ui->row->addItem("5");
    ui->row->addItem("6");
    ui->row->addItem("7");
    ui->row->addItem("8");
    ui->row->addItem("9");
    ui->column->addItem("1");
    ui->column->addItem("2");
    ui->column->addItem("3");
    ui->column->addItem("4");
    ui->column->addItem("5");
    ui->column->addItem("6");
    ui->column->addItem("7");
    ui->column->addItem("8");
    ui->column->addItem("9");
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButton_clicked()
{
    int row = ui->row->currentText().toInt();
    int col = ui->column->currentText().toInt();
  QString s =ui->lineEdit->text();
    if(s==nullptr)
    {
        QMessageBox box;
        box.setText("视屏墙名字不能为空");
        box.exec();
        return;
    }
    emit rowandcol(row,col,s);
    accept();
}
