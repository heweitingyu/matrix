#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include<QListWidget>
#include<QListWidgetItem>
#include<QPushButton>
#include<QLayout>
#include<QLabel>
#include<QLineEdit>
#include<QVector>
#include<QPainter>
#include<QDebug>
#include<QTextCodec>
#include<windef.h>
#include<QTreeWidget>
#include<QTreeWidgetItem>
#include<QtMath>
#include<qmath.h>
#include <QNetworkAccessManager>    //加载网络请求头文件
#include <QNetworkReply>
#include <QNetworkRequest>
#include<QUdpSocket>
#include<QHostAddress>
#include <QContextMenuEvent>
#include<QMenu>
#include<QAction>
#include<QTimer>
#include<QToolButton>
#include<QToolBar>
#include<QThread>
#include<QTextEdit>
#include<QMessageBox>
#include<QPixmap>
#include<QButtonGroup>
#include<QSettings>
#include<QComboBox>
#include<QtSerialPort/QSerialPort>
#include<QtSerialPort/QSerialPortInfo>
#include<QFileDialog>
#include<QRadioButton>
#include<QCheckBox>
#include<QProgressBar>
#include"windows.h"
#include"dialog.h"
#include"mywidget.h"
#include"mytreewidget.h"

namespace Ui {
class Widget;
}

struct mylabel
{
    int light_1_x;
    int light_1_y;
    int light_2_x;
    int light_2_y;
    int right_1_x;
    int right_1_y;
    int right_2_x;
    int right_2_y;

};
struct myshipinqiang
{
    QTreeWidgetItem *panduanitem; //单击切换的变量
    int lie,hang;   //视频墙行列
    QStringList peizhi;  //配置的输出节点名称
    QStringList weizhi;  //配备的对应的位置
    QString name;
};
Q_DECLARE_METATYPE(myshipinqiang)
class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();
        Dialog      *dialog;
signals:
        void chonghua(int,int,int,int);
public slots:
    void initui();
    void creatvideowallSignal();
    void createvideowall(int row,int col,QString mingzi);
    void createvideowall_switch(int,int);
    void itemclicked(QTreeWidgetItem *,int);
    void itemClicked(QTreeWidgetItem *,int);
    void GainIn();
    void GainOut();
    void oneProcessFinished(QNetworkReply *reply);
    void Receive(QString);
    void LightupScreen(QString,QPoint);
    void sendinstructionsfun();
    bool eventFilter(QObject *watched, QEvent *event);
    QTreeWidgetItem *  AddRoot(QTreeWidgetItem *parent,QString name);
   void writedata(QList<int>,int,bool);
   void MatrixRingCut(QPoint begin,QPoint end);
   void AutoSplicing();
   void TabWidgetWhichTabIsDown(int );
   void wallclear();
   void AutoSplicingSignal();
   void bianhuan(QVector<QRect>,QStringList);
   void binding();
   void _RESET();
   void OverScan();
   void ScanSetup();
   void ScanSetupone();
   void winclose();
   void closeEvent(QCloseEvent *event);
   void saveacg();
   void loadacg();
   void createvideowall_load(int,int,QString,QStringList,QStringList);
   void Othersetup();
   void sendintervalfun();
   void deletenode();
   void shanchushipinqiang();
   void tianjiazimu();
   void fsleixing(int);
   void dakaichuankou();
   void ReadData();
   void shengji();
   void shengjigujian();
   void kuaisuqiehuan();
   void querenqiehuan();
   void qiehuanclose();
   QByteArray TOHEX(int);
   int TOINT(QByteArray);
   void shengjiclose();
   void updateprogress(int);
   void qitaclose();
private:
    Ui::Widget *ui;

    //界面
    mywidget          *widget;
    QTabWidget *tabwidget;
    QTabWidget *tabwidget_1;
    mytreewidget  *treewidget;
    mytreewidget  *treewidget_1;
    QTreeWidgetItem *treewidgetItem;
    QTreeWidgetItem  *item;
    QTreeWidgetItem *item_1;
    QTreeWidgetItem *item_2;
    QTreeWidgetItem *qita;
    QTreeWidgetItem *UpgradeItem;
    QTreeWidgetItem *Setup;
    QPushButton *sendinstructions;
    QPushButton *bindingIP;
    QPushButton *RESET;
    QGridLayout *gridlayout;
    QLineEdit *edit_1;
    QLineEdit *edit;
    QVector<QList<int>> caijian;
    QList<int> labelweizhi;

    //过扫描
    QPushButton *closeButton;
    QVector<QLabel *> saomiaolabel;
    QVector<QLineEdit *> saomiaoedit;
    QButtonGroup *btn;
    QWidget *hidewidget;
    QPushButton *SHEZHI;
    QGridLayout *gridlayout_1;
    int aa[25];

    //升级固件
    QWidget *shengjiwidget;
    QPushButton *shengjiButton;
    QButtonGroup *shengjigroup;
    QPushButton *closeshengji;

    QTreeWidgetItem *qiehuan;
    QWidget *ksqiehuan;

    QVector<QLabel *> Label;
    QVector<QList<int> *> qingp;

    QTreeWidgetItem *item_3;
    QTreeWidgetItem *baocunweizhi;
    QButtonGroup *qiehuanButton;
    QPushButton *qsButton;
    QComboBox *danchulv;
    QRadioButton *danrudanchu;
    QPushButton *qhcloseButton;

    QNetworkAccessManager *manage;

    QUdpSocket *clientudp;
    bool shuchu=true;
    bool shuru=false;
    QTimer *ptime;
    QToolBar *tool;
    QPushButton *zidongpingjie;
    QPushButton *qingpin;
    QPushButton *shanchu;
    QPushButton *shezhizimu;
    bool pingjie = true;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    bool shoudao = true;
    bool draw = false;
    bool queren = false;
    QPixmap _pixmap;
    QVector<QRect> rect;
    QStringList _str;
    QList<int> _num;
    QStringList _STR;

    QWidget *qitawidget;

    QPushButton *pushbutton1;
    QStringList sss;

    QLineEdit *shuimianedit;
    QLineEdit *zitikuandu;
    QLineEdit *zitichangdu;
    QLineEdit *wenzi;
    QLineEdit *zimu;
    QLineEdit *ziti1;
    QLineEdit *ziti2;
    int ROW=0;
    int COL=0;
    unsigned long sendinterval = 100;
    bool huitu = false;

    QLabel   *label_1;
    QLabel   *label_2;
    QList<int> *array;
    int k =0;
    struct myshipinqiang video[20];
    QComboBox *sendtype;
    QComboBox *com;
    QComboBox *buda_rate;
    QPushButton *opencom;
    bool chuankou = false;
    QHBoxLayout *hboxlayout_1;
    QLabel *comc;
    QLabel *rate;
    QSerialPort *serial_l;
    bool serialopen = false;
    QProgressBar *progressbar;
    QPushButton *closeqita;
};

#endif // WIDGET_H
