#ifndef BANGDING_H
#define BANGDING_H

#include <QDialog>

namespace Ui {
class bangding;
}

class bangding : public QDialog
{
    Q_OBJECT

public:
    explicit bangding(QWidget *parent = nullptr);
    ~bangding();
private slots:
 void Bang(QString,int,int);
 void clicked();

private:
    Ui::bangding *ui;
};

#endif // BANGDING_H
