#include "widget.h"
#include "ui_widget.h"
#include"mytreewidget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    QFile file(":/qss/widget_style.qss");
    file.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(file.readAll());
    qApp->setStyleSheet(styleSheet);
    file.close();
    QTextCodec::setCodecForLocale(QTextCodec::codecForLocale());
    dialog = new Dialog(this);
   clientudp = new QUdpSocket(this);
   ptime = new QTimer(this);
   clientudp->bind(2000,QUdpSocket::ShareAddress);
    setWindowState(Qt::WindowMaximized);
    initui();
    loadacg();
    qDebug()<<k;
    for(int i =0;i<=24;i++)
    {
        aa[i]=0;
    }
    zidongpingjie->installEventFilter(this);
    label_3->installEventFilter(this);
    qingpin->installEventFilter(this);
    label_4->installEventFilter(this);
    shanchu->installEventFilter(this);
    label_5->installEventFilter(this);
    foreach (const QSerialPortInfo &info,QSerialPortInfo::availablePorts())
    {
        QSerialPort serial;
        serial.setPort(info);
        if(serial.open(QIODevice::ReadWrite))
        {
            com->addItem(serial.portName());
            serial.close();
        }
    }
    manage = new QNetworkAccessManager(this);       //分配空间
    //serial_l = new QSerialPort(this);
    connect(manage,SIGNAL(finished(QNetworkReply*)),this,SLOT(oneProcessFinished(QNetworkReply*)));     //绑定完成信号
    connect(widget,SIGNAL(ping()),this,SLOT(AutoSplicingSignal()));
    connect(dialog,SIGNAL(rowandcol(int,int,QString)),this,SLOT(createvideowall(int,int,QString)));
    connect(widget,SIGNAL(beginandend(QPoint,QPoint)),this,SLOT(MatrixRingCut(QPoint,QPoint)));
    connect(treewidget,SIGNAL(itemDoubleClicked(QTreeWidgetItem *,int)),this,SLOT(itemclicked(QTreeWidgetItem *,int)));
    connect(treewidget_1,SIGNAL(itemDoubleClicked(QTreeWidgetItem *,int)),this,SLOT(itemclicked(QTreeWidgetItem *,int)));
    connect(treewidget_1,SIGNAL(itemClicked(QTreeWidgetItem * , int)),this,SLOT(itemClicked(QTreeWidgetItem *,int)));
    connect(treewidget,SIGNAL(itemClicked(QTreeWidgetItem * , int)),this ,SLOT(itemClicked(QTreeWidgetItem * , int)));
    connect(ptime, SIGNAL(timeout()), this, SLOT(update()));
    connect(zidongpingjie,SIGNAL(clicked()),this,SLOT(AutoSplicing()));
    connect(tabwidget, SIGNAL(currentChanged(int)), this, SLOT(TabWidgetWhichTabIsDown(int )));
    connect(qingpin,SIGNAL(clicked()),this,SLOT(wallclear()));
    connect(sendinstructions,SIGNAL(clicked()),this,SLOT(sendinstructionsfun()));
    connect(widget,SIGNAL(quedingweizhi(QString,QPoint)),this,SLOT(LightupScreen(QString,QPoint)));
    connect(widget,SIGNAL(RECT(QVector<QRect>,QStringList)),this,SLOT(bianhuan(QVector<QRect>,QStringList)));
    connect(bindingIP,SIGNAL(clicked()),this,SLOT(binding()));
    connect(RESET,SIGNAL(clicked()),this,SLOT(_RESET()));
    connect(SHEZHI,SIGNAL(clicked()),this,SLOT(ScanSetup()));
    connect(closeButton,SIGNAL(clicked()),this,SLOT(winclose()));
    connect(closeshengji,SIGNAL(clicked()),this,SLOT(shengjiclose()));
    connect(shanchu,SIGNAL(clicked()),this,SLOT(deletenode()));
    connect(sendtype,SIGNAL(currentIndexChanged(int)),this,SLOT(fsleixing(int)));
    connect(opencom,SIGNAL(clicked()),this,SLOT(dakaichuankou()));
    connect(shengjiButton,SIGNAL(clicked()),this,SLOT(shengji()));
    connect(qsButton,SIGNAL(clicked()),this,SLOT(querenqiehuan()));
    connect(qhcloseButton,SIGNAL(clicked()),this,SLOT(qiehuanclose()));
    connect(closeqita,SIGNAL(clicked()),this,SLOT(qitaclose()));
}

Widget::~Widget()
{
    delete ui;
}

void Widget::initui()
{
    gridlayout = new QGridLayout(this);
    gridlayout->setContentsMargins(8,8,8,8);
    gridlayout->setSpacing(3);
    zidongpingjie = new QPushButton;
    qingpin = new QPushButton;
    shanchu = new QPushButton;
    tool = new QToolBar;
    QLabel *jianju = new QLabel();
    jianju->setMinimumWidth(5);
    jianju->setMaximumWidth(5);
    QLabel *jianju1 = new QLabel();
    jianju1->setMinimumWidth(5);
    jianju1->setMaximumWidth(5);
    tool->addWidget(zidongpingjie);
    tool->addWidget(jianju);
    tool->addWidget(qingpin);
    tool->addWidget(jianju1);
    tool->addWidget(shanchu);
    widget = new mywidget(this);
    tabwidget = new QTabWidget(this);
    treewidget = new mytreewidget(this);
    treewidget->setColumnCount(1);
    treewidget_1 = new mytreewidget(this);
    treewidget_1->setColumnCount(2);
    treewidget_1->setColumnWidth(0,260);
    treewidget->setFocusPolicy(Qt::NoFocus);
    treewidget_1->setFocusPolicy(Qt::NoFocus);
    tabwidget->insertTab(0,treewidget,"平台配置");
    tabwidget->insertTab(1,treewidget_1,"平台使用");
    progressbar = new QProgressBar(this);
    progressbar->setRange(0,100);
    progressbar->setValue(0);
    progressbar->hide();
    hidewidget = new QWidget;
    qitawidget = new QWidget;
    shengjiwidget = new QWidget;
    ksqiehuan = new QWidget;
    SHEZHI= new QPushButton(hidewidget);
    SHEZHI->setMaximumWidth(120);
    closeButton= new QPushButton(hidewidget);
    closeButton->setStyleSheet("QPushButton{border-image:url(:/qss/image/guanbi.png) 0 81 0 0 ;border-top-right-radius:3 ;}QPushButton:hover{border-image:url(:/qss/image/guanbi.png) 0 54 0 27 ;border-top-right-radius:3 ;} QPushButton:pressed{border-image:url(:/qss/image/guanbi.png) 0 27 0 54 ;border-top-right-radius:3 ;}");
    closeshengji = new QPushButton(shengjiwidget);
    closeshengji->setStyleSheet("QPushButton{border-image:url(:/qss/image/guanbi.png) 0 81 0 0 ;border-top-right-radius:3 ;}QPushButton:hover{border-image:url(:/qss/image/guanbi.png) 0 54 0 27 ;border-top-right-radius:3 ;} QPushButton:pressed{border-image:url(:/qss/image/guanbi.png) 0 27 0 54 ;border-top-right-radius:3 ;}");
    closeqita = new QPushButton(qitawidget);
    closeqita->setStyleSheet("QPushButton{border-image:url(:/qss/image/guanbi.png) 0 81 0 0 ;border-top-right-radius:3 ;}QPushButton:hover{border-image:url(:/qss/image/guanbi.png) 0 54 0 27 ;border-top-right-radius:3 ;} QPushButton:pressed{border-image:url(:/qss/image/guanbi.png) 0 27 0 54 ;border-top-right-radius:3 ;}");
    SHEZHI->setText("一键配置");
    hidewidget->setStyleSheet("QWidget{background:rgb(47,65,96);"+QString::fromUtf8("color: rgb(255, 255, 255);")+";}");
    hidewidget->hide();
    qitawidget->setStyleSheet("QWidget{background:rgb(47,65,96);"+QString::fromUtf8("color: rgb(255, 255, 255);")+";}");
    qitawidget->hide();
    shengjiwidget->setStyleSheet("QWidget{background:rgb(47,65,96);"+QString::fromUtf8("color: rgb(255, 255, 255);")+";}");
    shengjiwidget->hide();
    ksqiehuan->setStyleSheet("QWidget{background:rgb(47,65,96);"+QString::fromUtf8("color: rgb(255, 255, 255);")+";}");
    ksqiehuan->hide();
    item=new QTreeWidgetItem(QStringList());
    item->setText(0,"视频墙");
    treewidget->addTopLevelItem(item);
    item_1=new QTreeWidgetItem(QStringList());
    item_1->setText(0,"输入节点");
    treewidget_1->addTopLevelItem(item_1);
    item_2=new QTreeWidgetItem(QStringList());
    item_2->setText(0,"输出节点");
    item_3 = new QTreeWidgetItem;
    treewidget->addTopLevelItem(item_2);
//    QTreeWidgetItem * item3=new QTreeWidgetItem(QStringList()<<"0通道,HDMI,分辨率1");
//      item_2->addChild(item3);
//      QTreeWidgetItem * item4=new QTreeWidgetItem(QStringList()<<"1通道,HDMI,分辨率2");
//      item_2->addChild(item4);
//      QTreeWidgetItem * item8=new QTreeWidgetItem(QStringList()<<"2通道,HDMI,分辨率1");
//      item_2->addChild(item8);
//      QTreeWidgetItem * item7=new QTreeWidgetItem(QStringList()<<"3通道,HDMI,分辨率2");
//      item_2->addChild(item7);
//      QTreeWidgetItem * item5=new QTreeWidgetItem(QStringList()<<"0通道,HDMI");
//      item_1->addChild(item5);
//      QTreeWidgetItem * item6=new QTreeWidgetItem(QStringList()<<"1通道,HDMI");
//      item_1->addChild(item6);
    shengjiButton = new QPushButton(shengjiwidget);
    shengjiButton->setText("升级");
    shengjiButton->setMaximumWidth(120);
    qsButton = new QPushButton(ksqiehuan);
    qsButton->setText("切换");
    danrudanchu = new QRadioButton(ksqiehuan);
    danrudanchu->setText("淡入淡出");
    qiehuanButton = new QButtonGroup(ksqiehuan);
    qhcloseButton = new QPushButton(ksqiehuan);
    qhcloseButton->setStyleSheet("QPushButton{border-image:url(:/qss/image/guanbi.png) 0 81 0 0 ;border-top-right-radius:3 ;}QPushButton:hover{border-image:url(:/qss/image/guanbi.png) 0 54 0 27 ;border-top-right-radius:3 ;} QPushButton:pressed{border-image:url(:/qss/image/guanbi.png) 0 27 0 54 ;border-top-right-radius:3 ;}");
    widget->setMinimumWidth(1603);
    widget->setMaximumWidth(1603);
    widget->setMinimumHeight(903);
    widget->setMaximumHeight(903);
    widget->show();
    tabwidget->setMaximumWidth(300);
    treewidget->setMinimumHeight(800);
    treewidget->setMaximumWidth(300);
    QFont font;
    font.setPixelSize(30);
    item_1->setFont(0,font);
    item_2->setFont(0,font);
    item->setFont(0,font);
    item->setBackground(0,QColor(120, 170, 220));
    item_1->setBackground(0,QColor(120, 170, 220));
    item_1->setBackground(1,QColor(120, 170, 220));
    item_2->setBackground(0,QColor(120, 170, 220));
    item->setForeground(0,Qt::white);
     item_1->setForeground(0,Qt::white);
      item_2->setForeground(0,Qt::white);
      Setup = new QTreeWidgetItem(QStringList()<<"设置");
      treewidget->addTopLevelItem(Setup);
      Setup->setFont(0,font);
      Setup->setBackground(0,QColor(120,170,220));
      Setup->setForeground(0,Qt::white);
      qita = new QTreeWidgetItem(QStringList()<<"其它设置");
      QTreeWidgetItem *saomiaoban = new QTreeWidgetItem(QStringList()<<"过扫描");
      Setup->addChild(saomiaoban);
      Setup->addChild(qita);
      UpgradeItem = new QTreeWidgetItem(QStringList()<<"升级固件");
      Setup->addChild(UpgradeItem);
      bindingIP = new QPushButton(this);
      RESET = new QPushButton(this);
      bindingIP->setStyleSheet("QPushButton{border-radius: 4px;border: none;width: 75px;height: 25px;background: rgb(120, 170, 220);color: white;} QPushButton:hover{background: rgb(100, 160, 220);} QPushButton:pressed{background: rgb(0, 78, 161);}");
      RESET->setStyleSheet("QPushButton{border-radius: 4px;border: none;width: 75px;height: 25px;background: rgb(120, 170, 220);color: white;} QPushButton:hover{background: rgb(100, 160, 220);} QPushButton:pressed{background: rgb(0, 78, 161);}");
      bindingIP->setText("绑定");
      RESET->setText("复位");
      qiehuan = new QTreeWidgetItem(QStringList()<<"切换");
      Setup->addChild(qiehuan);
      danchulv = new QComboBox(ksqiehuan);
      danchulv->addItem("1");
      danchulv->addItem("2");
      danchulv->addItem("3");
      danchulv->addItem("4");
      danchulv->addItem("5");
      danchulv->addItem("6");
      danchulv->addItem("7");
      danchulv->addItem("8");
      danchulv->addItem("9");
      treewidget->setRootIsDecorated(true);
    treewidget->setHeaderHidden(true);
    treewidget_1->setMinimumHeight(700);
    treewidget_1->setMaximumWidth(350);
    treewidget_1->setRootIsDecorated(true);
    treewidget_1->setHeaderHidden(true);
    edit = new QLineEdit(this);
    edit_1 = new QLineEdit(this);
    label_1 = new QLabel(this);
    label_3=new QLabel(this);
    label_4=new QLabel(this);
    label_5=new QLabel(this);
    sendinstructions = new QPushButton(this);
    sendinstructions->setText("发送指令");
    sendinstructions->setStyleSheet("QPushButton{border-radius: 4px;border: none;width: 75px;height: 25px;background: rgb(120, 170, 220);color: white;} QPushButton:hover{background: rgb(100, 160, 220);} QPushButton:pressed{background: rgb(0, 78, 161);}");
    label_3->hide();
    label_3->setText("关闭自动补齐");
    label_4->hide();
    label_4->setText("清屏");
    label_5->hide();
    label_5->setText("删除");
    label_3->setStyleSheet("QLabel{background-color:rgb(120, 170, 220);border:0.5px solid rgba(173, 202, 232,50%)}");
    label_4->setStyleSheet("QLabel{background-color:rgb(120, 170, 220);border:0.5px solid rgba(173, 202, 232,50%)}");
    label_5->setStyleSheet("QLabel{background-color:rgb(120, 170, 220);border:0.5px solid rgba(173, 202, 232,50%)}");
    label_3->adjustSize();
    label_4->adjustSize();
    label_5->adjustSize();
    label_1->setText("输入IP地址");
    sendtype = new QComboBox(this);
    com = new QComboBox(this);
    buda_rate = new QComboBox(this);
    opencom = new QPushButton(this);
    comc = new QLabel(this);
    rate = new QLabel(this);
    sendtype->addItem("http连接");
    sendtype->addItem("串口连接");
    opencom->setText("打开串口");
    opencom->setStyleSheet("QPushButton{border-radius: 4px;border: none;width: 75px;height: 25px;background: rgb(120, 170, 220);color: white;} QPushButton:hover{background: rgb(100, 160, 220);} QPushButton:pressed{background: rgb(0, 78, 161);}");
    comc->setText("串口");
    rate->setText("波特率");
    buda_rate->addItem("115200");
    buda_rate->addItem("57600");
    buda_rate->addItem("38400");
    buda_rate->addItem("19200");
    buda_rate->addItem("14400");
    buda_rate->addItem("9600");
    QHBoxLayout *hboxlayout_3= new QHBoxLayout(this);
    hboxlayout_1 = new QHBoxLayout(this);
    hboxlayout_3->addWidget(sendtype);
    hboxlayout_1->addWidget(label_1);
    hboxlayout_1->addWidget(edit);
    hboxlayout_1->addWidget(bindingIP);
    comc->hide();
    com->hide();
    rate->hide();
    buda_rate->hide();
    opencom->hide();
    QHBoxLayout *hboxlayout_2 = new QHBoxLayout(this);
    hboxlayout_2->addWidget(RESET);
    hboxlayout_2->addWidget(sendinstructions);
    hboxlayout_2->addWidget(edit_1);
    hboxlayout_2->addStretch(1);
    edit->setMinimumWidth(150);
    edit->setMaximumWidth(150);
    edit_1->setMinimumWidth(140);
    gridlayout->addWidget(tool,0,0,1,1);
     gridlayout->addLayout(hboxlayout_3,1,0,1,1);
     gridlayout->addLayout(hboxlayout_1,2,0,1,1);
     gridlayout->addLayout(hboxlayout_2,3,0,1,1);
    gridlayout->addWidget(widget,1,3,4,1);
    gridlayout->addWidget(tabwidget,4,0,2,1);
    btn = new QButtonGroup();
    shengjigroup = new QButtonGroup();
}
void Widget::itemclicked(QTreeWidgetItem *citem,int c)
{
    c = 0;
    if(citem==item)
    {
        if(k!=0)
        {
            for(int i =0;i<19;i++)
            {
                if(baocunweizhi==video[i].panduanitem)
                {      video[i].peizhi.clear();
                       video[i].weizhi.clear();
                    for(int j =0; j<Label.size();j++)
                    {
                        if(Label.at(j)->text()!=nullptr)
                        {
                         video[i].peizhi.append(Label.at(j)->text());
                         video[i].weizhi.append(QString::number(j));
                        }
                    }
                }
            }
        }
      creatvideowallSignal();
    }
    if(citem==item_1)
    {
      GainIn();

    }
    if(citem==item_2)
    {
      GainOut();
    }
    if(citem->parent()==item_2)
    {
        item_3 = citem;
        QString s = citem->text(0).left(1);
        QString c = edit->text();
        if(!chuankou){
        QString z =("http://"+c+"/transmit?data=(MAPS"+s+")");
        QNetworkAccessManager manager;
         QNetworkReply *pReply = manager.get(QNetworkRequest(QUrl(z)));
         QEventLoop loop;
         QTimer timer;
         QObject::connect(pReply,SIGNAL(finished()),&loop,SLOT(quit()));
         QObject::connect(&timer,SIGNAL(timeout()),&loop,SLOT(quit()));
         timer.start(300);
         loop.exec();

         if (timer.isActive()){
             timer.stop();
             QString h =pReply->readAll();
         }
         else
         {
            disconnect(pReply,SIGNAL(finished()),&loop,SLOT(quit()));
            pReply->abort();
         }
        }
        else
        {
            QString m =("(MAPS"+s+")");
            serial_l->write(m.toLatin1());
        }
    }
    if(citem->text(0)=="过扫描")
    {
        OverScan();
    }
    if(citem->text(0)=="其它设置")
    {
        Othersetup();
    }
    if(citem->text(0)=="升级固件")
    {
        shengjigujian();
    }
    if(citem->text(0)=="切换")
    {
        kuaisuqiehuan();
    }
}
void Widget::itemClicked(QTreeWidgetItem *citem,int c)
{
    c = 0;
    if(citem!=item_3&&item_3!=nullptr)
    {
        QString c = edit->text();
        QString s =item_3->text(0).left(1);
        QString z =("http://"+c+"/transmit?data=(MAPE"+s+")");
        if(!chuankou){
        manage->get(QNetworkRequest(QUrl(z)));}
        else
        {
            QString m = ("(MAPE"+s+")");
            serial_l->write(m.toLatin1());
        }
        item_3 = nullptr;
    }
    if(citem->parent()==item_1)
    {
        widget->label(treewidget_1->currentItem()->text(0));
    }
    if(citem==item_1)
    {
        if(!item_1->isExpanded())
        {
        item_1->setExpanded(true);
        }
        else{item_1->setExpanded(false);}
    }
    if(citem==item)
    {
        if(!item->isExpanded())
        {
            item->setExpanded(true);
        }
        else{item->setExpanded(false);}
    }
    if(citem==item_2)
    {
        if(!item_2->isExpanded()){
        item_2->setExpanded(true);}
        else{
            item_2->setExpanded(false);
        }
    }
    if(citem==Setup)
    {
        if(!Setup->isExpanded())
        {
            Setup->setExpanded(true);}
        else
        {
            Setup->setExpanded(false);
        }
        }
    if(citem->parent()==item)
    {
        if(citem==baocunweizhi)
        {
            return;
        }
        for(int i =0;i<20;i++)
        {
            if(baocunweizhi==video[i].panduanitem)
            {
                widget->widgetqingping();
                qingp.clear();
                video[i].peizhi.clear();
                video[i].weizhi.clear();
           //     video[i].weizhi.clear();
          //      video[i].peizhi.clear();
                for(int j =0; j<Label.size();j++)
                {
                    if(Label.at(j)->text()!=nullptr)
                    {
                     video[i].peizhi.append(Label.at(j)->text());
                     video[i].weizhi.append(QString::number(j));
                    }
                }
            }
        }
        for(int i = 0;i<20 ;i++)
        {
            if(citem==video[i].panduanitem)
            {
                _str = video[i].peizhi;
           //     _num = video[i].weizhi;
                for(int j = 0;j<video[i].weizhi.size();j++)
                {
                    _num.append(video[i].weizhi.at(j).toInt());
                }
                createvideowall_switch(video[i].hang,video[i].lie);
                baocunweizhi = citem;
            }
        }
    }
}
void Widget::creatvideowallSignal()
{
   delete widget->layout();
    dialog->show();
}
void Widget::createvideowall(int row, int col,QString mingzi)
{
    for(int z =Label.size()-1;z>=0;z--)
    {
        Label.at(z)->deleteLater();
        Label.pop_back();
    }
    ROW = row;
    COL = col;
    int rowsize=0;
    int colsize=0;
if(col>=row)
{
    rowsize=1600/col;
    colsize=900/col;
}
else {
    rowsize = 1600/row;
    colsize = 900/row;
}
    QGridLayout *gridlayout_1 = new QGridLayout(widget);
    QHBoxLayout *hboxlayout_1 = new QHBoxLayout;
    QVBoxLayout *vboxlayout_1 = new QVBoxLayout;
    gridlayout_1->addLayout(vboxlayout_1,10,1,1,1);
    gridlayout_1->addLayout(hboxlayout_1,1,10,1,1);
    vboxlayout_1->addStretch(1);
    hboxlayout_1->addStretch(1);
    gridlayout_1->setSpacing(0);
    gridlayout_1->setContentsMargins(0, 0, 0, 0);
    for(int i = 1;i<=row;i++)
    {
        for(int j = 1;j<=col;j++)
        {
            QLabel *label1 = new QLabel(widget);
            label1->setAcceptDrops(true);
            Label.append(label1);
            label1->setMinimumWidth(rowsize);
            label1->setMaximumWidth(rowsize);
            label1->setMinimumHeight(colsize);
            label1->setMaximumHeight(colsize);
            label1->setStyleSheet("QLabel{background-color:rgba(232,241,252,100);border:0.5px solid rgba(173, 202, 232,50%)}");
            gridlayout_1->addWidget(label1,i,j,1,1);
        }
    }
    QTreeWidgetItem * item_4=new QTreeWidgetItem(QStringList()<<mingzi);
    item->addChild(item_4);
    treewidget->setCurrentItem(item_4);
    video[k].hang = ROW;
    video[k].lie = COL;
    video[k].panduanitem = item_4;
    video[k].name = mingzi;
    baocunweizhi = item_4;
    k++;
    widget->widgetqingping();
    qingp.clear();
}
void Widget::createvideowall_switch(int row,int col)
{
    delete widget->layout();
    for(int z =Label.size()-1;z>=0;z--)
    {
        Label.at(z)->deleteLater();
        Label.pop_back();
    }
    ROW = row;
    COL = col;
    int rowsize=0;
    int colsize=0;
    QFont font;
    font.setPixelSize(30);
if(col>=row)
{
    rowsize=1600/col;
    colsize=900/col;
}
else {
    rowsize = 1600/row;
    colsize = 900/row;
}
    QGridLayout *gridlayout_1 = new QGridLayout(widget);
    QHBoxLayout *hboxlayout_1 = new QHBoxLayout;
    QVBoxLayout *vboxlayout_1 = new QVBoxLayout;
    gridlayout_1->addLayout(vboxlayout_1,10,1,1,1);
    gridlayout_1->addLayout(hboxlayout_1,1,10,1,1);
    vboxlayout_1->addStretch(1);
    hboxlayout_1->addStretch(1);
    gridlayout_1->setSpacing(0);
    gridlayout_1->setContentsMargins(0, 0, 0, 0);
    for(int i = 1;i<=row;i++)
    {
        for(int j = 1;j<=col;j++)
        {
            QLabel *label2 = new QLabel(this);
            label2->setAcceptDrops(true);
            label2->setAlignment(Qt::AlignCenter);
            label2->setFont(font);
            Label.append(label2);
            label2->setMinimumWidth(rowsize);
            label2->setMaximumWidth(rowsize);
            label2->setMinimumHeight(colsize);
            label2->setMaximumHeight(colsize);
            label2->setStyleSheet("QLabel{background-color:rgba(232,241,252,100);border:0.5px solid rgba(173, 202, 232,50%)}");
            gridlayout_1->addWidget(label2,i,j,1,1);
        }
    }
    qDebug()<<Label.size();
    for(int i =0;i<_num.size();i++)
    {
        Label.at(_num.at(i))->setText(_str.at(i));
    }
    _str.clear();
    _num.clear();
}
void Widget::oneProcessFinished(QNetworkReply *reply)
{
    shoudao = true;
  //  qDebug()<<1;
    QString str = reply->readAll();
    qDebug()<<reply;
    qDebug()<<str;
    Receive(str);
}
void Widget::GainIn()
{
    shuru = true;
    shuchu = false;
    QString s = edit->text();
    QString z =("http://"+s+"/transmit?data=(GETIA)");
      int l = item_1->childCount();
      for(int i = 0; i<l;i++)
      {
          delete item_1->takeChild(0);
      }
      if(!chuankou)
      {
    manage->get(QNetworkRequest(QUrl(z)));
      }
      else
      {
          serial_l->write("(GETIA)");
      }
}
void Widget::GainOut()
{
    shuru = false;
    shuchu = true;
    QString s = edit->text();
    QString z =("http://"+s+"/transmit?data=(GETNOA)");
   // qDebug()<<z;
    int l = item_2->childCount();
    for(int i = 0; i<l;i++)
    {
        delete item_2->takeChild(0);
    }
    if(!chuankou){
    manage->get(QNetworkRequest(QUrl(z)));
    }
    else
    {
        serial_l->write("(GETOA)");
    }

}
void Widget::Receive(QString str)
{
   str.remove(QChar('('),Qt::CaseInsensitive);
   str.remove(QChar(')'),Qt::CaseInsensitive);
   if(str=="OK"&&queren)
   {
       for(int i =0;i<Label.size();i++)
       {
           Label.at(i)->setStyleSheet("background-color:rgba(232,241,252,100);border:0.5px solid rgba(173, 202, 232,50%)");
       }
   }
   QStringList s = str.split("|");
   qDebug()<<s;
   if(shuru)
   {
   for(int i=0;i<s.size();i++)
   {
       if(s.at(i)=="H1")
       {
           QTreeWidgetItem * item=new QTreeWidgetItem(QStringList()<<QString::number(i)+"通道"+",HDMI");
           item_1->addChild(item);
           item->setIcon(0,QIcon(":/qss/image/green.png"));

       }
       if(s.at(i)=="D1")
       {
QTreeWidgetItem * item=new QTreeWidgetItem(QStringList()<<QString::number(i)+"通道"+",DVI");
item_1->addChild(item);
 item->setIcon(0,QIcon(":/qss/image/green.png"));
       }
       if(s.at(i)=="V1")
       {
QTreeWidgetItem * item=new QTreeWidgetItem(QStringList()<<QString::number(i)+"通道"+",VGA");
item_1->addChild(item);
 item->setIcon(0,QIcon(":/qss/image/green.png"));
       }
       if(s.at(i)=="L1")
       {
QTreeWidgetItem * item=new QTreeWidgetItem(QStringList()<<QString::number(i)+"通道"+",LAN");
item_1->addChild(item);
 item->setIcon(0,QIcon(":/qss/image/green.png"));
       }
       if(s.at(i)=="C1")
       {
           QTreeWidgetItem * item=new QTreeWidgetItem(QStringList()<<QString::number(i)+"通道"+",CVBS");
           item_1->addChild(item);
            item->setIcon(0,QIcon(":/qss/image/green.png"));
       }
       if(s.at(i)=="S1")
       {
           QTreeWidgetItem * item=new QTreeWidgetItem(QStringList()<<QString::number(i)+"通道"+",SDI");
           item_1->addChild(item);
            item->setIcon(0,QIcon(":/qss/image/green.png"));
       }
       if(s.at(i)=="Y1")
       {
           QTreeWidgetItem * item=new QTreeWidgetItem(QStringList()<<QString::number(i)+"通道"+",YUV");
           item_1->addChild(item);
            item->setIcon(0,QIcon(":/qss/image/green.png"));
       }
       if(s.at(i)=="H0")
       {
AddRoot(item_1,QString::number(i)+"通道"+",HDMI");
       }
       if(s.at(i)=="D0")
       {
AddRoot(item_1,QString::number(i)+"通道"+",DVI");
       }
       if(s.at(i)=="V0")
       {
AddRoot(item_1,QString::number(i)+"通道"+",VGA");
       }
       if(s.at(i)=="L0")
       {
AddRoot(item_1,QString::number(i)+"通道"+",LAN");
       }
       if(s.at(i)=="C0")
       {
   AddRoot(item_1,QString::number(i)+"通道"+",CVBS");
       }
   if(s.at(i)=="S0")
   {
       AddRoot(item_1,QString::number(i)+"通道"+",SDI");
   }
   if(s.at(i)=="Y0")
   {
      AddRoot(item_1,QString::number(i)+"通道"+",YUV");
   }
    }
   }
   if(shuchu)
   {
       for(int i=0;i<s.size();i++)
       {
           if(s.at(i)=="H0")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"HDMI，分辨率为0");
           }
           if(s.at(i)=="H1")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"HDMI，分辨率为1");
           }
           if(s.at(i)=="H2")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"HDMI，分辨率为2");
           }
           if(s.at(i)=="H3")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"HDMI，分辨率为3");
           }
           if(s.at(i)=="H4")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"HDMI，分辨率为4");
           }
           if(s.at(i)=="H5")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"HDMI，分辨率为5");
           }
           if(s.at(i)=="H6")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"HDMI，分辨率为6");
           }
           if(s.at(i)=="H7")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"HDMI，分辨率为7");
           }
           if(s.at(i)=="H8")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"HDMI，分辨率为8");
           }
           if(s.at(i)=="H9")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"HDMI，分辨率为9");
           }
           if(s.at(i)=="D0")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"DVI，分辨率为0");
           }
           if(s.at(i)=="D1")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"DVI，分辨率为1");
           }
           if(s.at(i)=="D2")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"DVI，分辨率为2");
           }
           if(s.at(i)=="D3")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"DVI，分辨率为3");
           }
           if(s.at(i)=="D4")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"DVI，分辨率为4");
           }
           if(s.at(i)=="D5")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"DVI，分辨率为5");
           }
           if(s.at(i)=="D6")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"DVI，分辨率为6");
           }
           if(s.at(i)=="D7")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"DVI，分辨率为7");
           }
           if(s.at(i)=="D8")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"DVI，分辨率为8");
           }
           if(s.at(i)=="D9")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"DVI，分辨率为9");
           }
           if(s.at(i)=="V0")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"VGA，分辨率为0");
           }
           if(s.at(i)=="V1")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"VGA，分辨率为1");
           }
           if(s.at(i)=="V2")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"VGA，分辨率为2");
           }
           if(s.at(i)=="V3")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"VGA，分辨率为3");
           }
           if(s.at(i)=="V4")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"VGA，分辨率为4");
           }
           if(s.at(i)=="V5")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"VGA，分辨率为5");
           }
           if(s.at(i)=="V6")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"VGA，分辨率为6");
           }
           if(s.at(i)=="V7")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"VGA，分辨率为7");
           }
           if(s.at(i)=="V8")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"VGA，分辨率为8");
           }
           if(s.at(i)=="V9")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"VGA，分辨率为9");
           }
           if(s.at(i)=="L0")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"LAN，分辨率为0");
           }
           if(s.at(i)=="L1")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"LAN，分辨率为1");
           }
           if(s.at(i)=="L2")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"LAN，分辨率为2");
           }
           if(s.at(i)=="L3")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"LAN，分辨率为3");
           }
           if(s.at(i)=="L4")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"LAN，分辨率为4");
           }
           if(s.at(i)=="L5")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"LAN，分辨率为5");
           }
           if(s.at(i)=="L6")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"LAN，分辨率为6");
           }
           if(s.at(i)=="L7")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"LAN，分辨率为7");
           }
           if(s.at(i)=="L8")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"LAN，分辨率为8");
           }
           if(s.at(i)=="L9")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"LAN，分辨率为9");
           }
           if(s.at(i)=="S0")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"SDI，分辨率为0");
           }
           if(s.at(i)=="S1")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"SDI，分辨率为1");
           }
           if(s.at(i)=="S2")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"SDI，分辨率为2");
           }
           if(s.at(i)=="S3")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"SDI，分辨率为3");
           }
           if(s.at(i)=="S4")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"SDI，分辨率为4");
           }
           if(s.at(i)=="S5")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"SDI，分辨率为5");
           }
           if(s.at(i)=="S6")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"SDI，分辨率为6");
           }
           if(s.at(i)=="S7")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"SDI，分辨率为7");
           }
           if(s.at(i)=="S8")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"SDI，分辨率为8");
           }
           if(s.at(i)=="S9")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"SDI，分辨率为9");
           }
           if(s.at(i)=="C0")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"CVBS，分辨率为0");
           }
           if(s.at(i)=="C1")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"CVBS，分辨率为1");
           }
           if(s.at(i)=="C2")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"CVBS，分辨率为2");
           }
           if(s.at(i)=="C3")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"CVBS，分辨率为3");
           }
           if(s.at(i)=="C4")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"CVBS，分辨率为4");
           }
           if(s.at(i)=="C5")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"CVBS，分辨率为5");
           }
           if(s.at(i)=="C6")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"CVBS，分辨率为6");
           }
           if(s.at(i)=="C7")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"CVBS，分辨率为7");
           }
           if(s.at(i)=="C8")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"CVBS，分辨率为8");
           }
           if(s.at(i)=="C9")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"CVBS，分辨率为9");
           }
           if(s.at(i)=="Y0")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"YUV，分辨率为0");
           }
           if(s.at(i)=="Y1")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"YUV，分辨率为1");
           }
           if(s.at(i)=="Y2")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"YUV，分辨率为2");
           }
           if(s.at(i)=="Y3")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"YUV，分辨率为3");
           }
           if(s.at(i)=="Y4")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"YUV，分辨率为4");
           }
           if(s.at(i)=="Y5")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"YUV，分辨率为5");
           }
           if(s.at(i)=="Y6")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"YUV，分辨率为6");
           }
           if(s.at(i)=="Y7")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"YUV，分辨率为7");
           }
           if(s.at(i)=="Y8")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"YUV，分辨率为8");
           }
           if(s.at(i)=="Y9")
           {
    AddRoot(item_2,QString::number(i)+"通道"+"YUV，分辨率为9");
           }
       }
   }
}
QTreeWidgetItem *  Widget::AddRoot(QTreeWidgetItem *parent,QString name)
{
    QTreeWidgetItem * item=new QTreeWidgetItem(QStringList()<<name);
       parent->addChild(item);
     //  treewidget_1->setItemWidget(item,0,labell);
       return item;
}
void Widget::writedata(QList<int> s,int num,bool last)
{
    QByteArray shuju;
    if(!draw)
    {
        return;
    }
    if(treewidget_1->currentItem()==nullptr)
    {
        return;
    }
    QString q = treewidget_1->currentItem()->text(0).left(1);
    shuju.append("(");
    shuju.append("CROP");
    for(int i =0;i<s.size();i++)
    {
    shuju.append(QString("%1").arg(s.at(i),4,16,QLatin1Char('0')));
    }
    shuju.append("|");
    shuju.append(q);
    shuju.append(">");
   // QString c =  LABEL->text().left(1);
   // QString c =Label.at(0)->text().left(1);
    if(_num.size()==0)
    {
        return;
    }
    if(!_num.contains(num))
    {
        return;
    }
    QString c = _str.at(_num.indexOf(num)).left(1);
   shuju.append(c);
   sss.append(c);
   if(last)
   {
       shuju.append("|E");
       QString zzz;
       if(sss.size()<10&&sss.size()>=0)
       {
           zzz = "0"+QString::number(sss.size());
       }
       shuju.append(zzz);
       sss.clear();

   }
    shuju.append(")");
    if(!chuankou){
    QString z = edit->text();
    QString l =("http://"+z+"/transmit?data="+shuju);
    qDebug()<<l;
   // manage->get(QNetworkRequest(QUrl(l)));
    QUdpSocket Clientudp;
    QHostAddress address(z);
    Clientudp.writeDatagram(shuju,address,54321);
    }
    else
    {
        serial_l->write(shuju);
    }
    Sleep(sendinterval);
}
void Widget::MatrixRingCut(QPoint begin,QPoint end)
{
    QList<int> *qingchu = new QList<int>;
    QList<int> *cun = new QList<int>;
    int x,y,x1,y1;
    x=begin.x();
    y=begin.y();
    x1=end.x();
    y1=end.y();
    if(x>x1)
    {
        int tmp;
        tmp = x1;
        x1 = x;
        x = tmp;
    }
    if(y>y1)
    {
        int tmp;
        tmp = y1;
        y1 = y;
        y = tmp;
    }
  //  qDebug()<<x<<y<<x1<<y1;
    int rowsize;
               int colsize;
               if(COL>=ROW)
               {
                   rowsize=1600/COL;
                   colsize=900/COL;
               }
               else
               {
                   rowsize = 1600/ROW;
                   colsize = 900/ROW;
               }
    struct mylabel point[25];
    int jishu =0;
    for(int i =1;i<=ROW;i++)
    {
        for(int j =1;j<=COL;j++){

        point[jishu].light_1_x=rowsize*(j-1);
        point[jishu].light_1_y=colsize*(i-1);
        point[jishu].light_2_x=rowsize*(j-1);
        point[jishu].light_2_y=colsize*i;
        point[jishu].right_1_x=rowsize*j;
        point[jishu].right_1_y=colsize*(i-1);
        point[jishu].right_2_x=rowsize*j;
        point[jishu].right_2_y=colsize*i;
         jishu++;
        }
    }
    if(x1>point[COL*ROW-1].right_1_x)
    {
        x1=point[COL*ROW-1].right_1_x;
        widget->chonghui(x,y,x1,y1);
    }
    if(y1>point[COL*ROW-1].right_2_y)
    {
        y1=point[COL*ROW-1].right_2_y;
        widget->chonghui(x,y,x1,y1);
    }
    if(pingjie)
    {
        for(int i = 0;i<ROW*COL;i++)
        {
            if(x>=point[i].light_1_x&&x<point[i].right_2_x&&y>=point[i].light_1_y&&y<point[i].light_2_y)
            {
                x=point[i].light_1_x;
                y=point[i].light_1_y;
            }
            if(x1>point[i].light_1_x&&x1<=point[i].right_2_x&&y1>point[i].light_1_y&&y1<=point[i].light_2_y)
            {
                x1=point[i].right_2_x;
                y1=point[i].right_2_y;
            }
        }
        widget->chonghui(x,y,x1,y1);
    }
    int width = x1-x;
    int heith = y1-y;
    if(width<=200||heith<=150)
    {
        rect.removeLast();
        widget->rectmodify(rect,_STR);
        return;
    }
    qDebug()<<x<<y<<x1<<y1;
    int flag = 1;
    int flag_1=1;
    int flag_2=1;
    int flag_3=1;
    int m1=0,m2=0,m3=0,m4=0;
    for(int i =0;i<ROW*COL;i++)
    {   //shoudao = false;
        if(point[i].light_1_x>x&&point[i].light_1_x<x1&&point[i].light_1_y>y&&point[i].light_1_y<y1)
        {
            flag=-1;
        }else{flag=1;}
        if(point[i].light_2_x>x&&point[i].light_2_x<x1&&point[i].light_2_y>y&&point[i].light_2_y<y1)
        {
            flag_1=-1;
        }
else{flag_1=1;}
        if(point[i].right_1_x>x&&point[i].right_1_x<x1&&point[i].right_1_y>y&&point[i].right_1_y<y1)
        {
            flag_2=-1;
        }
else{flag_2=1;}
        if(point[i].right_2_x>x&&point[i].right_2_x<x1&&point[i].right_2_y>y&&point[i].right_2_y<y1)
        {
            flag_3=-1;
        }
else{flag_3=1;}
        if(flag==-1&&flag_1==1&&flag_2==1&&flag_3==1)
        {
              QList<int> s;
              s.append(point[i].light_1_x-x);
              s.append(width);
              s.append(point[i].light_1_y-y);
              s.append(heith);
              s.append(x1-point[i].light_1_x);
              s.append(width);
              s.append(y1-point[i].light_1_y);
              s.append(heith);
              s.append(0);
              s.append(1);
              s.append(0);
              s.append(1);
              s.append(x1-point[i].light_1_x);
              s.append(rowsize);
              s.append(y1-point[i].light_1_y);
              s.append(colsize);
          //    writedata(s,i,true);
              caijian.append(s);
              labelweizhi.append(i);
              qDebug()<<s;
              qingchu->append(i);
              shoudao = false;
        }
        if(flag==-1&&flag_1==-1&&flag_2==1&&flag_3==1)
        {
            QList<int> s;
          s.append(point[i].light_1_x-x);
          s.append(width);
          s.append(point[i].light_1_y-y);
          s.append(heith);
          s.append(x1-point[i].light_1_x);
          s.append(width);
          s.append(colsize);
          s.append(heith);
        s.append(0);
        s.append(1);
        s.append(0);
        s.append(1);
        s.append(x1-point[i].light_1_x);
        s.append(rowsize);
        s.append(1);
        s.append(1);
       // writedata(s,i,false);
        caijian.append(s);
        labelweizhi.append(i);
        qingchu->append(i);
        qDebug()<<s;
        shoudao = false;
        }
        if(flag==-1&&flag_1==-1&&flag_2==-1&&flag_3==-1)
        {
            QList<int> s;
            s.append(point[i].light_1_x-x);
            s.append(width);
            s.append(point[i].light_1_y-y);
            s.append(heith);
            s.append(rowsize);
            s.append(width);
            s.append(colsize);
            s.append(heith);
            s.append(0);
            s.append(1);
            s.append(0);
            s.append(1);
            s.append(1);
            s.append(1);
            s.append(1);
            s.append(1);
                     //    writedata(s,i,false);
            caijian.append(s);
            labelweizhi.append(i);
                         qingchu->append(i);
            qDebug()<<s;
            shoudao = false;
        }
        if(flag==1&&flag_1==-1&&flag_2==1&&flag_3==1)
        {
            QList<int> s;
            s.append(point[i].light_1_x-x);
            s.append(width);
            s.append(0);
            s.append(1);
            s.append(x1-point[i].light_1_x);
            s.append(width);
            s.append(point[i].light_2_y-y);
            s.append(heith);
            s.append(0);
            s.append(1);
            s.append(point[i].light_2_y-y);
            s.append(colsize);
            s.append(x1-point[i].light_1_x);
            s.append(rowsize);
            s.append(point[i].light_2_y-y);
            s.append(colsize);
                       //   writedata(s,i,false);
            caijian.append(s);
            labelweizhi.append(i);
                          qingchu->append(i);
            qDebug()<<s;
            shoudao = false;
        }
        if(flag==1&&flag_1==1&&flag_2==-1&&flag_3==1)
        {
          QList<int> s;
          s.append(0);
          s.append(1);
          s.append(point[i].light_1_y-y);
          s.append(heith);
          s.append(point[i].right_1_x-x);
          s.append(width);
          s.append(y1-point[i].light_1_y);
          s.append(heith);
          s.append(x);
          s.append(rowsize);
          s.append(0);
          s.append(1);
          s.append(point[i].right_1_x-x);
          s.append(rowsize);
          s.append(y1-point[i].right_1_y);
          s.append(colsize);
                     // writedata(s,i,false);
          caijian.append(s);
          labelweizhi.append(i);
                      qingchu->append(i);
          qDebug()<<s;
          shoudao = false;

        }
        if(flag==1&&flag_1==1&&flag_2==1&&flag_3==-1)
        {
        QList<int> s;
        s.append(0);
        s.append(1);
        s.append(0);
        s.append(1);
        s.append(point[i].right_1_x-x);
        s.append(width);
        s.append(point[i].light_2_y-y);
        s.append(heith);
        s.append(x-point[i].light_1_x);
        s.append(rowsize);
        s.append(y-point[i].light_1_y);
        s.append(colsize);
        s.append(point[i].right_1_x-x);
        s.append(rowsize);
        s.append(point[i].light_2_y-y);
        s.append(colsize);
      //  writedata(s,i,false);
        caijian.append(s);
        labelweizhi.append(i);
        qingchu->append(i);
        qDebug()<<s;
        shoudao = false;
        }
        if(flag==1&&flag_1==-1&&flag_2==1&&flag_3==-1)
        {
                    QList<int> s;
                    s.append(point[i].light_1_x-x);
                    s.append(width);
                    s.append(0);
                    s.append(1);
                    s.append(rowsize);
                    s.append(width);
                    s.append(point[i].light_2_y-y);
                    s.append(heith);
                    s.append(0);
                    s.append(1);
                    s.append(y-point[i].light_1_y);
                    s.append(colsize);
                    s.append(1);
                    s.append(1);
                    s.append(point[i].light_2_y-y);
                    s.append(colsize);
                 //   writedata(s,i,false);
                    caijian.append(s);
                    labelweizhi.append(i);
                    qDebug()<<s;
                    qingchu->append(i);
                    shoudao = false;
        }
        if(flag==-1&&flag_1==1&&flag_2==-1&&flag_3==1)
        {
                QList<int> s;
                s.append(point[i].light_1_x-x);
                s.append(width);
                s.append(point[i].light_1_y-y);
                s.append(heith);
                s.append(rowsize);
                s.append(width);
                s.append(y1-point[i].light_1_y);
                s.append(heith);
                s.append(0);
                s.append(1);
                s.append(0);
                s.append(1);
                s.append(1);
                s.append(1);
                s.append(y1-point[i].light_1_y);
                s.append(colsize);
                qingchu->append(i);
               //        writedata(s,i,false);
                caijian.append(s);
                labelweizhi.append(i);
                qDebug()<<s;
                shoudao = false;
        }
        if(flag==1&&flag_1==1&&flag_2==-1&&flag_3==-1)
        {
                  QList<int> s;
                  s.append(0);
                  s.append(1);
                  s.append(point[i].light_1_y-y);
                  s.append(heith);
                  s.append(point[i].right_1_x-x);
                  s.append(width);
                  s.append(colsize);
                  s.append(heith);
                  s.append(x-point[i].light_1_x);
                  s.append(rowsize);
                  s.append(0);
                  s.append(1);
                  s.append(point[i].right_1_x-x);
                  s.append(rowsize);
                  s.append(1);
                  s.append(1);
                  qDebug()<<s;
                  qingchu->append(i);
                //   writedata(s,i,false);
                  caijian.append(s);
                  labelweizhi.append(i);
                   shoudao = false;
        }

    }
    if(flag==1&&flag_1==1&&flag_2==1&&flag_3==1)
    {
        for(int i= 0;i<ROW*COL;i++)
        {
            if(x>=point[i].light_1_x&&x<point[i].right_1_x&&y>=point[i].light_1_y&&y<point[i].light_2_y)
            {
              m1=i;
            }
            if(x>=point[i].light_1_x&&x<point[i].right_1_x&&y1>point[i].light_1_y&&y1<=point[i].light_2_y)
            {
              m2=i;
            }
            if(x1>point[i].light_1_x&&x1<=point[i].right_1_x&&y>=point[i].light_1_y&&y<point[i].light_2_y)
            {
              m3=i;
            }
            if(x1>point[i].light_1_x&&x1<=point[i].right_1_x&&y1>point[i].light_1_y&&y1<=point[i].light_2_y)
            {
              m4=i;
            }
        }
        if(m1==m2&&m1!=m3)
        {
            for(int n=m1;n<=m3;n++)
            {
                if(n==m1)
                {
                     QList<int> s;
                     s.append(0);
                     s.append(1);
                     s.append(0);
                     s.append(1);
                     s.append(point[n].right_1_x-x);
                     s.append(width);
                     s.append(1);
                     s.append(1);
                     s.append(x-point[n].light_1_x);
                     s.append(rowsize);
                     s.append(y-point[n].light_1_y);
                     s.append(colsize);
                     s.append(point[n].right_1_x-x);
                     s.append(rowsize);
                     s.append(heith);
                     s.append(colsize);
                    // writedata(s,n,false);
                     caijian.append(s);
                     labelweizhi.append(n);
                     qDebug()<<s;
                     qingchu->append(n);
                }
                if(n==m3)
                {
                    QList<int> s;
                    s.append(point[n].light_1_x-x);
                    s.append(width);
                    s.append(0);
                    s.append(1);
                    s.append(x1-point[n].light_1_x);
                    s.append(width);
                    s.append(1);
                    s.append(1);
                    s.append(0);
                    s.append(1);
                    s.append(y-point[n].light_1_y);
                    s.append(colsize);
                    s.append(x1-point[n].light_1_x);
                    s.append(rowsize);
                    s.append(heith);
                    s.append(colsize);
                    //writedata(s,n,true);
                    caijian.append(s);
                    labelweizhi.append(n);
                    qDebug()<<s;
                    qingchu->append(n);
                }
                if(n>m1&&n<m3)
                {
                    QList<int> s;
                    s.append(point[n].light_1_x-x);
                    s.append(width);
                    s.append(0);
                    s.append(1);
                    s.append(rowsize);
                    s.append(width);
                    s.append(1);
                    s.append(1);
                    s.append(0);
                    s.append(1);
                    s.append(y-point[n].light_1_y);
                    s.append(colsize);
                    s.append(1);
                    s.append(1);
                    s.append(heith);
                    s.append(colsize);
                 //   writedata(s,n,false);
                    caijian.append(s);
                    labelweizhi.append(n);
                    qDebug()<<s;
                     qingchu->append(n);

                }

            }
        }
        if(m1==m2&&m1==m3)
        {
            QList<int> s;
            s.append(0);
            s.append(1);
            s.append(0);
            s.append(1);
            s.append(1);
            s.append(1);
            s.append(1);
            s.append(1);
            s.append(x-point[m1].light_1_x);
            s.append(rowsize);
            s.append(y-point[m1].light_1_y);
            s.append(colsize);
            s.append(width);
            s.append(rowsize);
            s.append(heith);
            s.append(colsize);
           // writedata(s,m1,true);
            caijian.append(s);
            labelweizhi.append(m1);
            qDebug()<<s;
             qingchu->append(m1);
        }
        if(m1==m3&&m1!=m2)
        {
            for(int n = m1;n<=m2;n=n+COL)
            {
                if(n==m1)
                {
                    QList<int> s;
                    s.append(0);
                    s.append(1);
                    s.append(0);
                    s.append(1);
                    s.append(1);
                    s.append(1);
                    s.append(point[n].light_2_y-y);
                    s.append(heith);
                    s.append(x-point[n].light_1_x);
                    s.append(rowsize);
                    s.append(y);
                    s.append(colsize);
                    s.append(width);
                    s.append(rowsize);
                    s.append(point[n].light_2_y-y);
                    s.append(colsize);
                   // writedata(s,m1,false);
                    caijian.append(s);
                    labelweizhi.append(n);
                    qDebug()<<s;
                     qingchu->append(n);
                }
                if(n==m2)
                {
                     QList<int> s;
                     s.append(0);
                     s.append(1);
                     s.append(point[n].light_1_y-y);
                     s.append(heith);
                     s.append(1);
                     s.append(1);
                     s.append(y1-point[n].light_1_y);
                     s.append(heith);
                     s.append(x-point[n].light_1_x);
                     s.append(rowsize);
                     s.append(0);
                     s.append(colsize);
                     s.append(width);
                     s.append(rowsize);
                     s.append(y1-point[n].light_1_y);
                     s.append(colsize);
                     //writedata(s,m2,true);
                     caijian.append(s);
                     labelweizhi.append(n);
                     qDebug()<<s;
                      qingchu->append(n);
                }
                if(n>m1&&n<m2)
                {
                      QList<int> s;
                      s.append(0);
                      s.append(1);
                      s.append(point[n].light_1_y-y);
                      s.append(heith);
                      s.append(1);
                      s.append(1);
                      s.append(colsize);
                      s.append(heith);
                      s.append(x-point[n].light_1_x);
                      s.append(rowsize);
                      s.append(0);
                      s.append(1);
                      s.append(width);
                      s.append(rowsize);
                      s.append(1);
                      s.append(1);
                      //writedata(s,n,false);
                      caijian.append(s);
                      labelweizhi.append(n);
                      qDebug()<<s;
                       qingchu->append(n);
                }
            }
        }
    }
    if(qingp.size()==0)
    {
    qingp.append(qingchu);
    }
    else
    {
    for(int h = 0;h<qingp.size();h++)
    {
        for(int i = 0;i<qingchu->size();i++){
        if(qingp.at(h)->contains(qingchu->at(i)))
        {
           cun->append(h);
            for(int j=0;j<qingp.at(h)->size();j++)
            {
                if(qingchu->contains(qingp.at(h)->at(j)))
                {
                    continue;
                }
                if(!_num.contains(qingp.at(h)->at(j)))
                {
                    continue;
                }
                QString t=_str.at(_num.indexOf(qingp.at(h)->at(j))).left(1);
               QString s = edit->text();
              //  QString z =("http://"+s+"/transmit?data=(CLR"+t+")");
              //  qDebug()<<z;
                if(!chuankou){
//                QNetworkAccessManager manager;
//                manager.get(QNetworkRequest(QUrl(z)));
               QUdpSocket Clientudp;
               QHostAddress address(s);
               QString z = "(CLR"+t+")";
               qDebug()<<z;
               Clientudp.writeDatagram(z.toUtf8(),address,54321);
                Sleep(30);
                }
                else
                {
                    QString m =("(CLR"+t+")");
                    serial_l->write(m.toLatin1());
                }
            }
                    break;
        }
        }
    }

  for(int j=cun->size()-1;j>=0;j--){
    qingp.remove(cun->at(j));
    rect.remove(cun->at(j));
    _STR.removeAt(cun->at(j));
    qDebug()<<rect.size()<<"矩阵个数";
     widget->rectmodify(rect,_STR);
    }
    qingp.append(qingchu);
    }
    for(int i = 0;i<qingp.size();i++)
    {
     qDebug()<<qingp.at(i)->size();
    }
    qDebug()<<caijian.size();
    for(int i = 0; i<caijian.size();i++)
    {
        if(i!=caijian.size()-1){
        writedata(caijian.at(i),labelweizhi.at(i),false);
        }
        else
        {
            writedata(caijian.at(i),labelweizhi.at(i),true);
        }
    }
    caijian.clear();
    labelweizhi.clear();
}
void Widget::AutoSplicing()
{
    if(pingjie)
    {
        pingjie=false;
        label_3->setText("自动补齐");
        label_3->adjustSize();
    }
    else
    {
        pingjie=true;
        label_3->setText("关闭自动补齐");
        label_3->adjustSize();
    }
    qDebug()<<pingjie;
}
void Widget::TabWidgetWhichTabIsDown(int l)
{
    if(l==1)
    {
        qDebug()<<"test";
        if(item_3!=nullptr)
        {
            QString c = edit->text();
            QString s =item_3->text(0).left(1);
            QString z =("http://"+c+"/transmit?data=(MAPE"+s+")");
            if(!chuankou){
            manage->get(QNetworkRequest(QUrl(z)));
            }
            else
            {
                QString m =("(MAPE"+s+")");
                serial_l->write(m.toLatin1());
            }
            item_3 = nullptr;
        }
        widget->on_huitu();
        draw = true;
        shuru = true;
        shuchu = false;
        QString s = edit->text();
        QString z =("http://"+s+"/transmit?data=(GETIA)");
        qDebug()<<z;
//        int l = item_1->childCount();
//        for(int i = 0; i<l;i++)
//        {
//            delete item_1->takeChild(0);
//        }
        if(!chuankou)
        {
        manage->get(QNetworkRequest(QUrl(z)));
        }
        else
        {
            QString m =("(GETIA)");
            serial_l->write(m.toLatin1());
        }
        for(int i =0; i<Label.size();i++)
        {
            if(Label.at(i)->text()!=nullptr)
            {
             _str.append(Label.at(i)->text());
             _num.append(i);
             Label.at(i)->setText("");
            }
        }
    }
    if(l==0)
    {
        qDebug()<<"TEST";
        widget->no_huitu();
        draw = false;
        shuru = false;
        shuchu = true;
        QString s = edit->text();
        QString z =("http://"+s+"/transmit?data=(GETNOA)");
        qDebug()<<z;
//        int l = item_2->childCount();
//        for(int i = 0; i<l;i++)
//        {
//            delete item_2->takeChild(0);
//        }
        if(!chuankou)
        {
        manage->get(QNetworkRequest(QUrl(z)));
        }
        else
        {
            QString m =("(GETIO)");
            serial_l->write(m.toLatin1());
        }
        for(int i =0;i<_num.size();i++)
        {
           Label.at(_num.at(i))->setText(_str.at(i));
        }
        _str.clear();
        _num.clear();
    }
}
bool Widget::eventFilter(QObject *watched, QEvent *event)
{
    if(zidongpingjie == watched || label_3 == watched) {
          if(QEvent::Enter == event->type()) {            //鼠标进入
              if (label_3->isHidden()) { //已经隐藏就显示出来
                  label_3->show();
                  QPoint point = zidongpingjie->pos();
                  point.rx() = point.x() -  label_3->width() + 105;
                  point.ry() = point.y() + zidongpingjie->height()+10;
                  label_3->move(point);
                  label_3->raise();//显示最顶层
                  return true;
              }
          }
          else if (QEvent::Leave == event->type())
          { //鼠标离开
              if (!label_3->isHidden())
              {
                      label_3->hide();
                      return true;
              }
          }
      }
    if(qingpin == watched || label_4 == watched)
    {
         if(QEvent::Enter == event->type())
         {
        if(label_4->isHidden())
          {
            label_4->show();
            QPoint point = qingpin->pos();
            point.rx() = point.x() - label_4->width() +65;
            point.ry() = point.y() + qingpin->height()+10;
            label_4->move(point);
            label_4->raise();
            return true;
          }
         }
        else if (QEvent::Leave == event->type())
         { //鼠标离开
            if (!label_4->isHidden())
            {
                    label_4->hide();
                    return true;
            }
        }
    }
    if(shanchu == watched || label_5 ==watched)
    {
        if(QEvent::Enter ==event->type())
        {
            if(label_5->isHidden())
            {
                label_5->show();
                QPoint point = shanchu->pos();
                point.rx() = point.x() - label_5->width() + 65;
                point.ry() = point.y() + shanchu->height()+ 10;
                label_5->move(point);
                label_5->raise();
                return true;
            }
        }
        else if (QEvent::Leave == event->type())
        {
            if(!label_5->isHidden())
            {
                label_5->hide();
                return true;
            }
        }
    }
       return QWidget::eventFilter(watched, event);
}
void Widget::wallclear()
{
    for(int i = 0;i <_str.size();i++)
    {

            QString x = _str.at(i).left(1);
            QString s = edit->text();
            if(!chuankou)
            {
             QString z =("http://"+s+"/transmit?data=(CLR"+x+")");
             QNetworkAccessManager manager;
             QNetworkReply *pReply = manager.get(QNetworkRequest(QUrl(z)));
             QEventLoop loop;
             QTimer timer;
             QObject::connect(pReply,SIGNAL(finished()),&loop,SLOT(quit()));
             QObject::connect(&timer,SIGNAL(timeout()),&loop,SLOT(quit()));
             timer.start(300);
             loop.exec();
             if (timer.isActive())
             {
                 timer.stop();
                 QString h =pReply->readAll();
                 qDebug()<<pReply;
                 qDebug()<<h;
             } else
             {
                 disconnect(pReply,SIGNAL(finished()),&loop,SLOT(quit()));
                 pReply->abort();
             }
            }
            else
            {
                QString m = ("(CLR"+x+")");
                serial_l->write(m.toLatin1());
            }
         //   Label.at(i)->clear();

    }
    qingp.clear();
    widget->widgetqingping();
    if(treewidget_1->currentItem()!=nullptr){
    widget->label(treewidget_1->currentItem()->text(0));
    }
    return ;

}
void Widget::AutoSplicingSignal()
{
    if(!pingjie)
    {
        pingjie=true;
        label_3->setText("关闭自动补齐");
        label_3->adjustSize();
    }
}
void Widget::sendinstructionsfun()
{
    if(!chuankou){
    QString s = edit_1->text();
    QString z = edit->text();
    QString l =("http://"+z+"/transmit?data=("+s+")");
     //manage->get(QNetworkRequest(QUrl(l)));
    QNetworkAccessManager manager;
     QNetworkReply *pReply = manager.get(QNetworkRequest(QUrl(l)));
     QEventLoop loop;
     QTimer timer;
     QObject::connect(pReply,SIGNAL(finished()),&loop,SLOT(quit()));
     QObject::connect(&timer,SIGNAL(timeout()),&loop,SLOT(quit()));
     // connect(pReply,SIGNAL(finished(QNetworkReply*)),this,SLOT(oneProcessFinished(QNetworkReply*)));
     timer.start(300);
     loop.exec();

     if (timer.isActive()){
         timer.stop();
       //  Label.at(i)->clear();
         QString h =pReply->readAll();
         qDebug()<<pReply;
         qDebug()<<h;
         if(h=="OK"){
         QMessageBox box;
         box.setText("发送成功");
         box.exec();
         }
         if(h=="ERROR")
         {
             QMessageBox box;
             box.setText("发送失败");
             box.exec();
         }
     } else {
         disconnect(pReply,SIGNAL(finished()),&loop,SLOT(quit()));
         pReply->abort();
         QMessageBox box;
         box.setText("回复超时");
         box.exec();
     }
    }
    else
    {
     QString m = "("+edit_1->text()+")";
     serial_l->write(m.toLatin1());
    }
}
void Widget::LightupScreen(QString str,QPoint begin)
{
    if(treewidget->currentItem()==nullptr||treewidget->currentItem()->parent()!=item_2)
    {
        return;
    }
 int x,y;
 x=begin.x();
 y=begin.y();
 QFont font;
 font.setPixelSize(30);
 int rowsize;
            int colsize;
            if(COL>=ROW)
            {
                rowsize=1600/COL;
                colsize=900/COL;
            }
            else
            {
                rowsize = 1600/ROW;
                colsize = 900/ROW;
            }
 struct mylabel point[25];
 int jishu =0;
 for(int i =1;i<=ROW;i++)
 {
     for(int j =1;j<=COL;j++)
     {
     point[jishu].light_1_x=rowsize*(j-1);
     point[jishu].light_1_y=colsize*(i-1);
     point[jishu].light_2_x=rowsize*(j-1);
     point[jishu].light_2_y=colsize*i;
     point[jishu].right_1_x=rowsize*j;
     point[jishu].right_1_y=colsize*(i-1);
     point[jishu].right_2_x=rowsize*j;
     point[jishu].right_2_y=colsize*i;
      jishu++;
     }
 }
 for(int i = 0;i < Label.size();i++)
 {
     if(x>point[i].light_1_x&&x<point[i].right_1_x&&y>point[i].light_1_y&&y<point[i].light_2_y)
     {
         for(int i =0;i<Label.size();i++)
         {
             if(Label.at(i)->text()==treewidget->currentItem()->text(0))
           {
                 Label.at(i)->setText("");
           }
         }
         Label.at(i)->setText(str);
         Label.at(i)->setText(treewidget->currentItem()->text(0));
         Label.at(i)->setFont(font);
         Label.at(i)->setAlignment(Qt::AlignCenter);
         QString s =Label.at(i)->text().left(1);
         QString c = edit->text();
         if(!chuankou)
         {
         QString z =("http://"+c+"/transmit?data=(MAPE"+s+")");
         manage->get(QNetworkRequest(QUrl(z)));
         }
         else
         {
             QString m =("(MAPE"+s+")");
             serial_l->write(m.toLatin1());
         }
     }
 }
}
void Widget::bianhuan(QVector<QRect> Rect,QStringList str)
{
    rect = Rect;
    _STR = str;
    qDebug()<<rect.size();
    qDebug()<<"test";
}
void Widget::binding()
{
    if(bindingIP->text()=="绑定")
    {
        qDebug()<<1;
     QString s = edit->text();
     QString z =("http://"+s+"/transmit?data=(GETIO)");
     manage->get(QNetworkRequest(QUrl(z)));

    edit->setEnabled(false);
    bindingIP->setText("解除绑定");
    }
    else
    {
       edit->setEnabled(true);
       bindingIP->setText("绑定");
    }
}
void Widget::_RESET()
{   if(!chuankou){
    QString s = edit->text();
    QString z = ("http://"+s+"/transmit?data=(reset)");
    manage->get(QNetworkRequest(QUrl(z)));
    }
    else {
        serial_l->write("(reset)");
    }
}
void Widget::kuaisuqiehuan()
{
    QGridLayout *gridLayout = new QGridLayout(ksqiehuan);
    int m = 0;
    int n = 0;
    for(int i = 0; i<item_2->childCount();i++)
    {
        QRadioButton *radioButton = new QRadioButton(ksqiehuan);
        radioButton->setText(item_2->child(i)->text(0).left(1)+"输出板卡");
        qiehuanButton->addButton(radioButton,i);
        if(n==3)
        {
            m++;
            n=0;
        }
        gridLayout->addWidget(radioButton,m,n,1,1);
        n++;
    }
    qiehuanButton->setExclusive(false);
    ksqiehuan->setGeometry(700,250,600,500);
    qhcloseButton->setGeometry(ksqiehuan->width()-25,5,20,20);
    gridLayout->addWidget(qsButton,8,7,1,1);
    gridLayout->addWidget(danrudanchu,7,7,1,1);
    gridLayout->addWidget(danchulv,7,8,1,1);
    ksqiehuan->raise();
    ksqiehuan->setWindowFlags(Qt::Dialog | Qt::FramelessWindowHint);
    ksqiehuan->setWindowModality(Qt::ApplicationModal);
    ksqiehuan->show();
    this->setEnabled(false);
}
void Widget::querenqiehuan()
{
    QUdpSocket client;

    for(int i = 0;i<qiehuanButton->buttons().size();i++)
    {
        if(qiehuanButton->button(i)->isChecked())
        {
            if(danrudanchu->isChecked())
            {
                QString s = "(SOB"+qiehuanButton->button(i)->text().left(1)+"|"+danchulv->currentText()+")";
                QHostAddress address(edit->text());
              client.writeDatagram(s.toUtf8(),address,54321);
            }
            else
            {
                QString s ="(SOB"+qiehuanButton->button(i)->text().left(1)+"|0)";
                QHostAddress address(edit->text());
                client.writeDatagram(s.toUtf8(),address,54321);
            }
        }
    }
}
void Widget::OverScan()
{
    int k = 0;
    int l = 0;
     gridlayout_1 = new QGridLayout(hidewidget);
    for(int i =0;i<item_2->childCount();i++)
    {
        QLabel *label = new QLabel(hidewidget);
        QLineEdit *Edit = new QLineEdit(hidewidget);
        Edit->setMaximumWidth(35);
        label->setMinimumWidth(35);
        QPushButton *pushbutton = new QPushButton(hidewidget);
        pushbutton->setText("配置");
        //Edit->setText("0");
        label->setText("板卡"+item_2->child(i)->text(0).left(1));
        Edit->setText(QString::number(aa[item_2->child(i)->text(0).left(1).toInt()]));
        saomiaolabel.append(label);
        saomiaoedit.append(Edit);
        btn->addButton(pushbutton,i);
        QHBoxLayout *hboxLayout = new QHBoxLayout();
        hboxLayout->addWidget(label);
        hboxLayout->addWidget(Edit);
        hboxLayout->addWidget(pushbutton);
        hboxLayout->addStretch();
        if(l==3)
        {
            k++;
            l=0;
        }
        gridlayout_1->addLayout(hboxLayout,k,l,1,1);
        l++;
        connect(pushbutton,SIGNAL(clicked()),this,SLOT(ScanSetupone()));
    }
     QHBoxLayout *hboxLayout_1 = new QHBoxLayout();
     hboxLayout_1->addStretch();
     hboxLayout_1->addWidget(SHEZHI);
     hboxLayout_1->addStretch();
    gridlayout_1->addLayout(hboxLayout_1,item_2->childCount(),0,1,3);
    hidewidget->setGeometry(700,250,600,500);
    closeButton->setGeometry(hidewidget->width()-25,5,20,20);
    QLabel *tishi = new QLabel(hidewidget);
    tishi->setText("请填写-50~50之间的数字");
    tishi->setGeometry(25,60,120,20);
    qDebug()<<saomiaolabel.size();
    hidewidget->raise();
    hidewidget->setWindowFlags(Qt::Dialog | Qt::FramelessWindowHint);
    hidewidget->setWindowModality(Qt::ApplicationModal);
    hidewidget->show();
}
void Widget::ScanSetup()
{
    QString s = edit->text();
    for(int i=0;i<saomiaolabel.size();i++)
    {
        QString z = saomiaolabel.at(i)->text().mid(2,2);
        QString l = saomiaoedit.at(i)->text();
        int a = l.toInt();
        if(a>50||a<-50)
        {
            QMessageBox box;
            box.setText(saomiaolabel.at(i)->text()+"超出填写范围");
            box.exec();
            continue ;
        }
        if(a>-10&&a<10)
        {
         l = ("0"+QString::number(a));
        }
      //  qDebug()<<z<<l;
        aa[z.toInt()]=a;
        if(!chuankou){
        QString p =("http://"+s+"/transmit?data=(SCAN"+z+"|"+l+")");
        QNetworkAccessManager manager;
         QNetworkReply *pReply = manager.get(QNetworkRequest(QUrl(p)));
         QEventLoop loop;
         QTimer timer;
         QObject::connect(pReply,SIGNAL(finished()),&loop,SLOT(quit()));
         QObject::connect(&timer,SIGNAL(timeout()),&loop,SLOT(quit()));
         timer.start(300);
         loop.exec();
         if (timer.isActive())
         {
             timer.stop();
             QString h =pReply->readAll();
             qDebug()<<pReply;
             qDebug()<<h;
         } else
          {
             disconnect(pReply,SIGNAL(finished()),&loop,SLOT(quit()));
             pReply->abort();
          }
        }
        else
        {
            QString m = ("(SCAN"+z+"|"+l+")");
            serial_l->write(m.toLatin1());
        }
}
    hidewidget->hide();
    saomiaolabel.clear();
    saomiaoedit.clear();
}
void Widget::ScanSetupone()
{
    QPushButton *s = qobject_cast<QPushButton *>(sender());
    int ID=btn->id(s);
    qDebug()<<ID;
     QString a = edit->text();
     QString z = saomiaolabel.at(ID)->text().mid(2,2);
     QString l = saomiaoedit.at(ID)->text();
     int b = l.toInt();
     if(b>50||b<-50)
     {
         QMessageBox box;
         box.setText("超出填写范围");
         box.exec();
         return ;
     }
     if(b>-10&&b<10)
     {
         l = ("0"+QString::number(b));
     }
     aa[z.toInt()]=b;
     if(!chuankou){
     QString p =("http://"+a+"/transmit?data=(SCAN"+z+"|"+l+")");
     qDebug()<<p;
     QNetworkAccessManager manager;
      QNetworkReply *pReply = manager.get(QNetworkRequest(QUrl(p)));
      QEventLoop loop;
      QTimer timer;
      QObject::connect(pReply,SIGNAL(finished()),&loop,SLOT(quit()));
      QObject::connect(&timer,SIGNAL(timeout()),&loop,SLOT(quit()));
      timer.start(300);
      loop.exec();
      if (timer.isActive()){
          timer.stop();
          QString h =pReply->readAll();
          qDebug()<<pReply;
          qDebug()<<h;
      } else {
          disconnect(pReply,SIGNAL(finished()),&loop,SLOT(quit()));
          pReply->abort();
      }
     }
     else
     {
         QString m = ("(SCAN"+z+"|"+l+")");
         serial_l->write(m.toLatin1());
     }
}
void Widget::Othersetup()
{
    QGridLayout *gridlayout4 = new QGridLayout(qitawidget);
    shezhizimu = new QPushButton(qitawidget);
    shezhizimu->setText("确定");
    QLabel *label = new QLabel(qitawidget);
    label->setText("设置发送间隔  填写范围为50-200");
     label->setMinimumWidth(35);
    shuimianedit = new QLineEdit(qitawidget);
    shuimianedit->setMaximumWidth(35);
    shuimianedit->setText(QString::number(sendinterval));
    pushbutton1 = new QPushButton(qitawidget);
    pushbutton1->setText("确定");
    QHBoxLayout *hboxLayout = new QHBoxLayout(qitawidget);
    hboxLayout->addWidget(label);
    hboxLayout->addWidget(shuimianedit);
    hboxLayout->addWidget(pushbutton1);
    hboxLayout->addStretch();
    connect(pushbutton1,SIGNAL(clicked()),this,SLOT(sendintervalfun()));
    QLabel *label1 = new QLabel(qitawidget);
    QLabel *label2 = new QLabel(qitawidget);
    label1->setText("字体宽度");
    label2->setText("字体高度");
    zimu = new QLineEdit(qitawidget);
    ziti1 = new QLineEdit(qitawidget);
    ziti2 = new QLineEdit(qitawidget);
    zimu->setMinimumWidth(250);
    ziti1->setMaximumWidth(30);
    ziti2->setMaximumWidth(30);
    QHBoxLayout *hboxLayout1 = new QHBoxLayout(qitawidget);
    hboxLayout1->addWidget(zimu);
    hboxLayout1->addWidget(label1);
    hboxLayout1->addWidget(ziti1);
    hboxLayout1->addWidget(label2);
    hboxLayout1->addWidget(ziti2);
    hboxLayout1->addWidget(shezhizimu);
    hboxLayout1->addStretch();
    gridlayout4->addLayout(hboxLayout,0,0,1,1);
    gridlayout4->addLayout(hboxLayout1,1,0,1,1);
    connect(shezhizimu,SIGNAL(clicked()),this,SLOT(tianjiazimu()));
    qitawidget->setGeometry(700,250,600,500);
    closeqita->setGeometry(qitawidget->width()-25,5,20,20);
    qitawidget->raise();
    qitawidget->setWindowFlags(Qt::Dialog | Qt::FramelessWindowHint);
    qitawidget->setWindowModality(Qt::ApplicationModal);
    qitawidget->show();
    this->setEnabled(false);
}
void Widget::sendintervalfun()
{
    if(shuimianedit->text().toInt()>200||shuimianedit->text().toInt()<50)
    {
        QMessageBox box;
        box.setText("超出填写范围");
        box.exec();
        return ;
    }
    sendinterval = shuimianedit->text().toULong();
  //  qDebug()<<fasongjiange;
    delete qitawidget->layout();
    qitawidget->hide();
}
void Widget::winclose()
{
    this->setEnabled(true);
    hidewidget->hide();
    saomiaolabel.clear();
    saomiaoedit.clear();
    for(int i = btn->buttons().size();i>=0;i--)
    {
        btn->button(i)->deleteLater();
    }
    delete hidewidget->layout();
}
void Widget::shengjiclose()
{
    this->setEnabled(true);
    progressbar->hide();
    progressbar->setValue(0);
    shengjiwidget->hide();
    for(int i=shengjigroup->buttons().size()-1;i>=0;i--)
    {
        shengjigroup->button(i)->deleteLater();
    }
    delete shengjiwidget->layout();
}
void Widget::qiehuanclose()
{
    this->setEnabled(true);
    ksqiehuan->hide();
    for(int i = qiehuanButton->buttons().size()-1;i>=0;i--)
    {
        qiehuanButton->button(i)->deleteLater();
    }
    delete ksqiehuan->layout();
}
void Widget::qitaclose()
{
    this->setEnabled(true);
    delete qitawidget->layout();
    qitawidget->hide();
}
void Widget::closeEvent(QCloseEvent *event)//关闭时自动保存
{
    if(!shengjiwidget->isHidden()||!hidewidget->isHidden()||!qitawidget->isHidden()||!ksqiehuan->isHidden())
    {
        event->ignore();
    }else{
        QString str = ("config/shipinqiang.ini");
        QString get = str.toUtf8();
        QFile::remove(get);
        saveacg();
        event->accept();
    }
}
void Widget::saveacg()
{
    QSettings sfg("config/shipinqiang.ini",QSettings::IniFormat);
    for(int i = 0 ;i<k ;i++)
    {
        sfg.setValue(video[i].name+"/hangshu",video[i].hang);
        sfg.setValue(video[i].name+"/lieshu",video[i].lie);
        sfg.setValue(video[i].name+"/peizhi",video[i].peizhi);
        sfg.setValue(video[i].name+"/weizhi",video[i].weizhi);
    }
    sfg.sync();
}
void Widget::loadacg()
{
    QSettings cfg("config/shipinqiang.ini",QSettings::IniFormat);
    QStringList groups = cfg.childGroups();
  //  qDebug()<<groups;
    for(int i =0;i<groups.size();i++)
    {
     cfg.beginGroup(groups.at(i));
     video[i].peizhi=cfg.value("peizhi").toStringList();
     video[i].weizhi=cfg.value("weizhi").toStringList();
     video[i].hang = cfg.value("hangshu").toInt();
     video[i].lie = cfg.value("lieshu").toInt();
     cfg.endGroup();
     video[i].name = groups.at(i);
     createvideowall_load(video[i].hang,video[i].lie,video[i].name,video[i].peizhi,video[i].weizhi);
     k++;
    }
}
void Widget::createvideowall_load(int row,int col,QString mingzi,QStringList peizhi,QStringList weizhi)
{
    delete widget->layout();
    for(int z =Label.size()-1;z>=0;z--)
    {
        Label.at(z)->deleteLater();
        Label.pop_back();
    }
    ROW = row;
    COL = col;
    int rowsize=0;
    int colsize=0;
    QFont font;
    font.setPixelSize(30);
if(col>=row)
{
    rowsize=1600/col;
    colsize=900/col;
}
else {
    rowsize = 1600/row;
    colsize = 900/row;
}
    QGridLayout *gridlayout_1 = new QGridLayout(widget);
    QHBoxLayout *hboxlayout_1 = new QHBoxLayout;
    QVBoxLayout *vboxlayout_1 = new QVBoxLayout;
    gridlayout_1->addLayout(vboxlayout_1,10,1,1,1);
    gridlayout_1->addLayout(hboxlayout_1,1,10,1,1);
    vboxlayout_1->addStretch(1);
    hboxlayout_1->addStretch(1);
    gridlayout_1->setSpacing(0);
    gridlayout_1->setContentsMargins(0, 0, 0, 0);
    for(int i = 1;i<=row;i++)
    {
        for(int j = 1;j<=col;j++)
        {
            QLabel *label3 = new QLabel(this);
            label3->setAcceptDrops(true);
            label3->setAlignment(Qt::AlignCenter);
            label3->setFont(font);
            Label.append(label3);
            label3->setMinimumWidth(rowsize);
            label3->setMaximumWidth(rowsize);
            label3->setMinimumHeight(colsize);
            label3->setMaximumHeight(colsize);
            label3->setStyleSheet("QLabel{background-color:rgba(232,241,252,100);border:0.5px solid rgba(173, 202, 232,50%)}");
            gridlayout_1->addWidget(label3,i,j,1,1);
        }
    }
    QTreeWidgetItem * item_4=new QTreeWidgetItem(QStringList()<<mingzi);
    item->addChild(item_4);
    treewidget->setCurrentItem(item_4);
     video[k].panduanitem = item_4;
     baocunweizhi = item_4;
     for(int i =0;i<weizhi.size();i++)
     {
        Label.at(weizhi.at(i).toInt())->setText(peizhi.at(i));
        Label.at(weizhi.at(i).toInt())->setStyleSheet("QLabel{background-color:rgba(232,241,252,100);border:0.5px solid rgba(173, 202, 232,50%)}");
     }
}
void Widget::deletenode()
{
   // qDebug()<<1;
    if(shuchu)
    {
        if(treewidget->currentItem()->parent()==item)
        {
           // qDebug()<<222;
            int ok = QMessageBox::warning(this,tr("删除!"),
            "你确定删除视频墙"+treewidget->currentItem()->text(0)+"吗?",QMessageBox::Yes, QMessageBox::No);
            if(ok == QMessageBox::No)
            {
            return;
            }
            else
            {
                shanchushipinqiang();
                int i = treewidget->currentIndex().row();
                delete item->takeChild(treewidget->currentIndex().row());
                if(item->childCount()!=0&&item->childCount()>i)
                {
                    itemClicked(item->child(i),0);
                }
                if(item->childCount()!=0&&item->childCount()<=i)
                {
                    itemClicked(item->child(i-1),0);
                }
                if(item->childCount()==0)
                {
                    delete widget->layout();
                    for(int z =Label.size()-1;z>=0;z--)
                    {
                        Label.at(z)->deleteLater();
                        Label.pop_back();
                    }
                }
            }
        }
        if(treewidget->currentItem()->parent()==item_2)
        {
            int ok = QMessageBox::warning(this,tr("删除!"),
            "你确定删除"+treewidget->currentItem()->text(0).left(1)+"输出通道吗?",QMessageBox::Yes, QMessageBox::No);
            if(ok == QMessageBox::No)
            {
                return;
            }
            else
            {
                delete item_2->takeChild(treewidget->currentIndex().row());
            }
        }
    }
    if(shuru)
    {
        if(treewidget_1->currentItem()->parent()==item_1)
        {
            int ok = QMessageBox::warning(this,tr("删除!"),
             "你确定删除"+treewidget_1->currentItem()->text(0).left(1)+"输入通道吗?",QMessageBox::Yes,QMessageBox::No);
            if(ok==QMessageBox::No)
            {
                return;
            }
            else
            {
                delete item_1->takeChild(treewidget_1->currentIndex().row());
            }
        }
    }
}
void Widget::shanchushipinqiang()
{
    for(int i =0;i<19;i++)
    {
        if(baocunweizhi==video[i].panduanitem)
        {
            video[i].panduanitem = video[k-1].panduanitem;
            video[i].hang = video[k-1].hang;
            video[i].lie = video[k-1].lie;
            video[i].weizhi = video[k-1].weizhi;
            video[i].peizhi = video[k-1].peizhi;
            video[i].name = video[k-1].name;
            video[k-1].panduanitem = nullptr;
            video[k-1].hang = 0;
            video[k-1].lie = 0;
            video[k-1].weizhi.clear();
            video[k-1].peizhi.clear();
            video[k-1].name = nullptr;
            k--;
            wallclear();
            return;
        }
    }
}
void Widget::tianjiazimu()
{
    QString s =zimu->text();
    int m =ziti1->text().toInt();
    int n =ziti2->text().toInt();
    QString p = edit->text();
    for(int i =0;i<s.size();i++)
    {
        QSize size(m,n);
        QImage image(size,QImage::Format_ARGB32);
        image.fill(qRgba(0,0,0,100));
        QPainter painter(&image);
        QPen pen = painter.pen();
        QFont font =painter.font();
        pen.setWidth(1);
        pen.setColor(Qt::white);
        font.setPixelSize(m-2);
        painter.setPen(pen);
        painter.setFont(font);
        painter.drawText(image.rect(),Qt::AlignCenter,s.at(i));
        image.save(QString::number(i)+".bmp","BMP");
        QFile f(QString::number(i)+".bmp");
        QByteArray data;
        if(f.open(QIODevice::ReadOnly))
        {
            data =f.readAll();
            f.close();
        }
        QByteArray data1 = data.toHex();
        QByteArray data2 = data1.mid(108);
        QString z =data2;
        QStringList sz;
        int weishu = (m*3+3)/4*4*2;
        for(int j =0;j<n;j++)
        {
            sz.append(z.mid(j*weishu,weishu));
        }
        for(int j = 0;j<sz.size()/2;j++)
        {
            sz.swap(j,sz.size()-j-1);
        }
        for(int i =0;i<sz.size();i++)
        {
            QString kk;
            QString ll;
            for(int j =0;j<m;j++)
            {
              QString zs =  sz.at(i).mid(j*6,6);
              if(zs=="000000")
              {
                  kk.append("00");
              }
              else
              {
                  kk.append("11");
              }
            }
            for(int o=0;o<kk.size()/4;o++)
            {
                bool ok;
                QString p =kk.mid(o*4,4);
                int w =p.toInt(&ok,2);
                QString e;
                e.setNum(w,16);
                ll.append(e);
            }
            qDebug()<<ll;
            if(!chuankou){
            QString l =("http://"+p+"/transmit?data="+ll);
            manage->get(QNetworkRequest(QUrl(l)));
            }
            else
            {
                ll.push_front("(");
                ll.push_back(")");
                serial_l->write(ll.toLatin1());
            }
        }
    }
}
void Widget::fsleixing(int i)
{
    i = 0;
    com->clear();
    if(sendtype->currentText()=="串口连接")
    {
        foreach (const QSerialPortInfo &info,QSerialPortInfo::availablePorts())
        {
            QSerialPort serial;
            serial.setPort(info);
            if(serial.open(QIODevice::ReadWrite))//打开串口
            {
                qDebug()<<2;
                com->addItem(serial.portName());
                serial.close();
            }
        }
        QLayoutItem *child;
        chuankou = true;
        while((child=hboxlayout_1->takeAt(0))!=nullptr){}
            label_1->hide();
            edit->hide();
            bindingIP->hide();
            comc->show();
            com->show();
            rate->show();
            buda_rate->show();
            opencom->show();
            hboxlayout_1->addWidget(comc);
            hboxlayout_1->addWidget(com);
            hboxlayout_1->addWidget(rate);
            hboxlayout_1->addWidget(buda_rate);
            hboxlayout_1->addWidget(opencom);
            hboxlayout_1->update();

    }
    else
    {
        if(serialopen)
        {
            serialopen = false;
            serial_l->clear();
            serial_l->close();
            serial_l->deleteLater();
        }
        //恢复设置使能
        com->setEnabled(true);
        buda_rate->setEnabled(true);
        opencom->setText(tr("打开串口"));
        QLayoutItem *child;
        chuankou = false;
        while((child=hboxlayout_1->takeAt(0))!=nullptr){}
              comc->hide();
              com->hide();
              rate->hide();
              buda_rate->hide();
              opencom->hide();
              label_1->show();
              edit->show();
              bindingIP->show();
              hboxlayout_1->addWidget(label_1);
              hboxlayout_1->addWidget(edit);
              hboxlayout_1->addWidget(bindingIP);
              hboxlayout_1->update();
    }
}
void Widget::dakaichuankou()
{
    if(opencom->text() == tr("打开串口"))
    {
        serial_l = new QSerialPort;
        serial_l->setPortName(com->currentText());
        serial_l->open(QIODevice::ReadWrite);
        serial_l->setBaudRate(buda_rate->currentText().toInt());
        serial_l->setDataBits(QSerialPort::Data8);
        serial_l->setParity(QSerialPort::NoParity);
        serial_l->setStopBits(QSerialPort::OneStop);
        serial_l->setFlowControl(QSerialPort::NoFlowControl);

        //关闭设置菜单使能
        com->setEnabled(false);
        buda_rate->setEnabled(false);
        opencom->setText(tr("关闭串口"));
        serialopen = true;
        connect(serial_l,SIGNAL(readyRead()),this,SLOT(ReadData()));
    }
    else
    {
        //关闭串口
        serial_l->clear();
        serial_l->close();
        serial_l->deleteLater();
        serialopen = false;
        //恢复设置使能
        com->setEnabled(true);
        buda_rate->setEnabled(true);
        opencom->setText(tr("打开串口"));
    }
}
void Widget::ReadData()
{
    QByteArray data;
    data = serial_l->readAll();
    qDebug()<<data;
    QString str = data;
    Receive(str);
}
void Widget::shengji()//传送bin文件
{
    QString filepath = QFileDialog::getOpenFileName(this,"open file",QDir::currentPath(),tr(("bin File(*.bin)")));
    if(filepath.isEmpty())
      {
          //QMessageBox::information(this,"Error Message", "Please Select a Bin File");
          return;
      }
    QFile file(filepath);
    if(!file.open(QIODevice::ReadOnly))
    {
        return;
    }
   progressbar->setGeometry(700,198,600,30);
   progressbar->show();
    progressbar->raise();
    QByteArray bytearray;
    bytearray = file.readAll();
    int baoshu = (bytearray.size()+256-1)/256;
    QString key;
    key = QString("%1").arg(bytearray.size(),8,16,QLatin1Char('0'));
    file.close();
    QString s = edit->text();
    for(int j = 0;j<shengjigroup->buttons().size();j++)
    {
       if(shengjigroup->button(j)->isChecked())
        {
               if(shengjiwidget->isHidden())
               {
                   return;
               }
    QString l = ("(UDM"+shengjigroup->button(j)->text().left(1)+"|50"+key+")");
    qDebug()<<l;
    QHostAddress address(s);
    QUdpSocket Clientudp;
    Clientudp.writeDatagram(l.toUtf8(),address,54321);
     QEventLoop loop;
     QObject::connect(&Clientudp,SIGNAL(readyRead()),&loop,SLOT(quit()));
      QObject::connect(closeshengji,SIGNAL(clicked()),&loop,SLOT(quit()));
     loop.exec();
     QByteArray data;
     data.resize(Clientudp.pendingDatagramSize());
     Clientudp.readDatagram(data.data(),data.size());
     QString op = data;
     qDebug()<<op<<1;
       while(data!="(OK)")
       {
           if(shengjiwidget->isHidden())
           {
               return;
           }
            QString l = ("(UDM"+shengjigroup->button(j)->text().left(1)+"|50"+key+")");
            QHostAddress address(s);
            QUdpSocket Clientudp;
            Clientudp.writeDatagram(l.toUtf8(),address,54321);
            QEventLoop loop;
            QObject::connect(&Clientudp,SIGNAL(readyRead()),&loop,SLOT(quit()));
             QObject::connect(closeshengji,SIGNAL(clicked()),&loop,SLOT(quit()));
            loop.exec();
             data.resize(Clientudp.pendingDatagramSize());
            Clientudp.readDatagram(data.data(),data.size());
            qDebug()<<data<<2;
       }

           qDebug()<<"ceshi1";
    for (int i =0;i<baoshu;i++)
    {
        qDebug()<<"ceshi2";
       QByteArray array;
       array =  bytearray.mid(i*256,256).toHex();
       QString shujuchangdu = QString("%1").arg(array.size()/2,8,16,QLatin1Char('0'));
       QString dizhi = QString("%1").arg(i*256,8,16,QLatin1Char('0'));
       int o = 81+shujuchangdu.mid(0,2).toInt(nullptr,16)+shujuchangdu.mid(2,2).toInt(nullptr,16)+shujuchangdu.mid(4,2).toInt(nullptr,16)+shujuchangdu.mid(6,2).toInt(nullptr,16)+dizhi.mid(0,2).toInt(nullptr,16)+dizhi.mid(2,2).toInt(nullptr,16)+dizhi.mid(4,2).toInt(nullptr,16)+dizhi.mid(6,2).toInt(nullptr,16);
       qDebug()<<o;
       for(int x =0;x<256;x++)
       {
         o = o+array.mid(x*2,2).toInt(nullptr,16);
       }
       qDebug()<<o;    
        QString ll = ("(UDM"+shengjigroup->button(j)->text().left(1)+"|51"+dizhi+shujuchangdu+array+TOHEX(o).toHex().mid(4,4)+")");
        qDebug()<<ll;
        QHostAddress address(s);
        QUdpSocket Clientudp;
        Clientudp.writeDatagram(ll.toUtf8(),address,54321);
       QEventLoop loop;
         QObject::connect(&Clientudp,SIGNAL(readyRead()),&loop,SLOT(quit()));
          QObject::connect(closeshengji,SIGNAL(clicked()),&loop,SLOT(quit()));
         loop.exec();
        QByteArray data;
        data.resize(Clientudp.pendingDatagramSize());
        Clientudp.readDatagram(data.data(),data.size());
        qDebug()<<data<<3;
          while(data!="(OK)")
          {
              if(shengjiwidget->isHidden())
              {
                  return;
              }
               i =0;
               QString l = ("(UDM"+shengjigroup->button(j)->text().left(1)+"|50"+key+")");
               QHostAddress address(s);
               QUdpSocket Clientudp;
               Clientudp.writeDatagram(l.toUtf8(),address,54321);
               QEventLoop loop;
               QObject::connect(&Clientudp,SIGNAL(readyRead()),&loop,SLOT(quit()));
               QObject::connect(closeshengji,SIGNAL(clicked()),&loop,SLOT(quit()));
               loop.exec();
                data.resize(Clientudp.pendingDatagramSize());
               Clientudp.readDatagram(data.data(),data.size());
               qDebug()<<data<<4;
          }
                 updateprogress((i+1)*100/baoshu);
    }
    }
       if(j==shengjigroup->buttons().size()-1&&progressbar->value()==100)
       {
       QMessageBox box;
       box.setText("升级完成");
       box.exec();
       }
    }
    return;
}
void Widget::shengjigujian()
{
    QGridLayout *gridLayout = new QGridLayout(shengjiwidget);
    int m =0;
    int n =0;
    for(int i = 0;i<item_2->childCount();i++){
    QRadioButton *radioButton = new QRadioButton(shengjiwidget);
    radioButton->setText(item_2->child(i)->text(0).left(1)+"输出板卡");
    shengjigroup->addButton(radioButton,i);
    if(n==2)
    {
        m++;
        n=0;
    }
    gridLayout->addWidget(radioButton,m,n,1,1);
    n++;
    }
    shengjigroup->setExclusive(false);
    gridLayout->addWidget(shengjiButton,8,8,1,1);
    shengjiwidget->setGeometry(700,250,600,500);
    closeshengji->setGeometry(shengjiwidget->width()-25,5,20,20);
    shengjiwidget->raise();
  //  shengjiwidget->setHidden(false);
    shengjiwidget->setWindowFlags(Qt::Dialog | Qt::FramelessWindowHint);
    shengjiwidget->setWindowModality(Qt::ApplicationModal);
    shengjiwidget->show();
    this->setEnabled(false);

}
QByteArray Widget::TOHEX(int i){
QByteArray abyte0;
   abyte0.resize(4);
   abyte0[3] = (uchar)  (0x000000ff & i);
   abyte0[2] = (uchar) ((0x0000ff00 & i) >> 8);
   abyte0[1] = (uchar) ((0x00ff0000 & i) >> 16);
   abyte0[0] = (uchar) ((0xff000000 & i) >> 24);
   return abyte0;
}
int Widget::TOINT(QByteArray bytes)
{
    int addr = bytes[0] & 0x000000FF;
    addr |= ((bytes[1] << 8) & 0x0000FF00);
    addr |= ((bytes[2] << 16) & 0x00FF0000);
    addr |= ((bytes[3] << 24) & 0xFF000000);
    return addr;
}
void Widget::updateprogress(int nCurrentvalue)
{
    progressbar->setValue(nCurrentvalue);
}
