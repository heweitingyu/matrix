#include "mywidget.h"

mywidget::mywidget(QWidget *parent):
    QWidget(parent)
{
    setAcceptDrops(true);
}
void mywidget::paintEvent(QPaintEvent *event)
{
    if(huihua){
     _pixmap =  QPixmap(size());
     _pixmap.fill(QColor(232,241,252));
    }
    int i =0;
     QPixmap pix = _pixmap;
     QPainter painter(&pix);
     QPoint point5;
     QFont font;
     font.setPixelSize(30);
     point1 = lastPoint;
     point2 = endPoint;
         painter.setBrush(QColor(0, 78, 161));
           painter.setFont(font);
         for(int c = 0;c<_shape.size();c++)
         {
        painter.drawRect(_rects.at(i));
        i++;
         }
         painter.setBrush(QColor(160,200,200));
         for(int c =0;c<_shape.size();c++)
         {
             point5.setX(_rects.at(c).topLeft().x());
             point5.setY(_rects.at(c).topLeft().y()+30);
             painter.drawRect(point5.x(),point5.y()-30,_rects.at(c).width(),30);
             painter.drawText(point5,_str.at(c));

         }
     //    painter.setFont(font);
         painter.end();
         painter.begin(this);
         painter.drawPixmap(0,0,pix);
}
void mywidget::mouseMoveEvent(QMouseEvent *event)
{
    if(!huihua)
    {
        return;
    }
    if(event->buttons()&Qt::LeftButton)
    {
         QRect &lastRect = _rects.last();
         lastRect.setBottomRight(event->pos());
          endPoint=event->pos();
          isdrawing = true;
          update();
    }
    if(event->button()==Qt::RightButton)
    {
        return;
    }
}
void mywidget::mousePressEvent(QMouseEvent *event)
{
    if(!huihua&&event->button()==Qt::LeftButton)
    {
        emit quedingweizhi("1",event->pos());
        return;
    }
    if(event->button()==Qt::LeftButton)  //当鼠标左键按下
       {
        QRect rect;
        _rects.append(rect);
        QRect &lastRect = _rects.last();
        lastRect.setTopLeft(event->pos());
        _shape.append(2);
        _str.append(s);
        lastPoint=event->pos();  //获得鼠标指针当前坐标作为起始坐标
        qDebug()<<_shape;
       }
    if(event->button()==Qt::RightButton)
    {
        return;
    }

}
void mywidget::mouseReleaseEvent(QMouseEvent *event)
{
    if(!huihua)
    {
        return;
    }
    if(fasong)
    {
        fasong = false;
        return ;
    }
    if(event->button()==Qt::LeftButton)  //如果鼠标左键释放
       {
           endPoint=event->pos();  //获得鼠标指针当前坐标作为终止坐标

         //  isdrawing=false;
           if(endPoint==lastPoint)
           {
               _rects.removeLast();
               _shape.removeLast();
               _str.removeLast();
               return;
           }
           QRect &lastRect = _rects.last();
           lastRect.setBottomRight(event->pos());
           emit RECT(_rects,_str);
           if(isdrawing)
           {
           emit beginandend(point1,point2);
           }
    }
    if(event->button()==Qt::RightButton)
    {
        return;
    }
}
void mywidget::chonghui(int x,int y,int x1,int y1)
{
  lastPoint.setX(x);
  lastPoint.setY(y);
  endPoint.setX(x1);
  endPoint.setY(y1);
  QRect &lastRect = _rects.last();
  lastRect.setTopLeft(lastPoint);
  lastRect.setBottomRight(endPoint);
  update();
  emit RECT(_rects,_str);
}
void mywidget::mouseDoubleClickEvent(QMouseEvent *event)
{
    for(int i = 0;i<_rects.size();i++){
    if(event->pos().x()>_rects.at(i).topLeft().x()&&event->pos().y()>_rects.at(i).topLeft().y()&&event->pos().x()<_rects.at(i).bottomRight().x()&&event->pos().y()<_rects.at(i).bottomRight().y())
    {
      //  qDebug()<<1;
        fasong = true;
        QPoint point3;
        QPoint point4;
        point3.setX(_rects.at(i).topLeft().x());
        point3.setY(_rects.at(i).topLeft().y());
        point4.setX(_rects.at(i).bottomRight().x());
        point4.setY(_rects.at(i).bottomRight().y());
        isdrawing=false;
        QRect rect;
        rect.setTopLeft(point3);
        rect.setBottomRight(point4);
        _rects.append(rect);
        _shape.append(2);
        _str.append(s);
        emit ping();
        emit RECT(_rects,_str);
        emit beginandend(point3,point4);
    }
    }
}
void mywidget::label(QString l)
{
    s=l;
}
void mywidget::dragEnterEvent(QDragEnterEvent *event)
{
    event->setDropAction(Qt::MoveAction);
    event->accept();
}
void mywidget::dragMoveEvent(QDragMoveEvent *event)
{
    event->setDropAction(Qt::MoveAction);
    event->accept();
}
void mywidget::dropEvent(QDropEvent *event)
{
    QString str = event->mimeData()->text();
    if (!str.isEmpty()&&huihua)
    {
        s = str;
     //   qDebug()<<str;
        emit ping();
        QRect rect;
        _rects.append(rect);
        _shape.append(2);
        _str.append(s);
        emit beginandend(event->pos(),event->pos());
        event->setDropAction(Qt::MoveAction);
        event->accept();
    }
    if(!str.isEmpty()&&!huihua)
    {
        emit quedingweizhi(str,event->pos());
        event->setDropAction(Qt::MoveAction);
        event->accept();
    }
}
void mywidget::on_huitu()
{
    huihua = true;
}
void mywidget::no_huitu()
{
    huihua = false;
}
void mywidget::rectmodify(QVector<QRect> RECT,QStringList str)
{
    _shape.removeFirst();
    _rects.clear();
    _str.clear();
    _rects   = RECT;
    _str = str;
    update();
}
void mywidget::widgetqingping()
{
    _rects.clear();
    _shape.clear();
    _str.clear();
    s="";
    update();
}
