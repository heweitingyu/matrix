#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include<QMessageBox>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();
signals:
      void rowandcol(int,int,QString);
private slots:
    void on_pushButton_clicked();
private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
