#include "mytreewidget.h"

mytreewidget::mytreewidget(QWidget *parent)
       :QTreeWidget (parent)
{
this->setAcceptDrops(true);
}
mytreewidget::~mytreewidget()
{

}
//重写鼠标点击操作.
void mytreewidget::mousePressEvent(QMouseEvent *event)
{
    //确保左键拖拽.
    if (event->button() == Qt::LeftButton)
    {
        //先保存拖拽的起点.
        m_dragPoint = event->pos();
        //保留被拖拽的项.
        m_dragItem = this->itemAt(event->pos());
    }
    //保留原QTreeWidget部件的鼠标点击操作.
    QTreeWidget::mousePressEvent(event);
}

void mytreewidget::mouseMoveEvent(QMouseEvent *event)
{
    //确保按住左键移动.
    if(m_dragItem==nullptr||m_dragItem->text(0)=="设置"||m_dragItem->text(0)=="输出"||m_dragItem->text(0)=="过扫描"||m_dragItem->text(0)=="其它"||m_dragItem->text(0)=="视频墙"||m_dragItem->text(0)=="输入")
    {
        return;
    }
    if (event->buttons() & Qt::LeftButton)
    {

        QPoint temp = event->pos() - m_dragPoint;
        //只有这个长度大于默认的距离,才会被系统认为是形成拖拽的操作.
        if (temp.manhattanLength() > QApplication::startDragDistance())
        {
           QDrag *drag = new QDrag(this);
           QMimeData *mimeData = new QMimeData;
           QPixmap drag_img(60,18);
           QPainter painter(&drag_img);
           painter.setPen(QColor(120,170,220));
           painter.setBrush(QColor(120,170,220));
               painter.drawRect(QRectF(0,0,60,18));
               drag->setPixmap(drag_img);
            mimeData->setText(m_dragItem->text(0));
            drag->setMimeData(mimeData);
            auto ACT = drag->exec(Qt::CopyAction | Qt::MoveAction);
            if (ACT == (Qt::CopyAction) || (ACT == Qt::MoveAction))
            {
                //当成功拖拽后，删除拖拽项.
               // auto i = this->takeItem(this->row(m_dragItem));
               // delete i;
            }
        }
    }
    QTreeWidget::mouseMoveEvent(event);
}

void mytreewidget::dragEnterEvent(QDragEnterEvent *event)
{
    //设置动作为移动动作.
    event->setDropAction(Qt::MoveAction);
    //然后接受事件.这个一定要写.
    event->accept();
}
void mytreewidget::dragMoveEvent(QDragMoveEvent *event)
{
    event->setDropAction(Qt::MoveAction);
    event->accept();
}
//当拖拽项被放下时的操作.
void mytreewidget::dropEvent(QDropEvent *event)
{
    QString str = event->mimeData()->text();
    if (!str.isEmpty())
    {
        //找到当前鼠标位置在部件中的项.
     //   auto item = this->itemAt(event->pos());
        //
       // if (!item)
         //   this->addItem(str);
       // else
         //   this->insertItem(this->row(item),str);

        event->setDropAction(Qt::MoveAction);
        event->accept();
    }
}
