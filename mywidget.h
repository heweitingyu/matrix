#ifndef MYWIDGET_H
#define MYWIDGET_H
#include<QWidget>
#include<QPainter>
#include<QMouseEvent>
#include<QDebug>
#include<QMimeData>
#include<QDrag>
#include<QFont>
#include<windef.h>
#include<windows.h>

class mywidget:public QWidget
{
    Q_OBJECT


public:
    mywidget(QWidget *parent = nullptr);
signals:
    void beginandend(QPoint,QPoint);
    void ping();
    void quedingweizhi(QString,QPoint);
    void RECT(QVector<QRect>,QStringList );
public slots:
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void chonghui(int,int,int,int);
    void mouseDoubleClickEvent(QMouseEvent *event);
    void label(QString l);
    void dragEnterEvent(QDragEnterEvent *event);
    void on_huitu();
    void no_huitu();
    void rectmodify(QVector<QRect>,QStringList);

    void dragMoveEvent(QDragMoveEvent *event);
    //当拖拽项被放下时的操作.
    void dropEvent(QDropEvent *event);
    void widgetqingping();
 //   void huatu(bool,bool);
public:
    bool isdrawing = true;
    bool huihua = false;
    bool fasong = false;
    QPoint lastPoint,endPoint;
    QPoint point1,point2;
    QString s;
    QPixmap _pixmap;
     QVector<QRect> _rects;//矩形集合
     QVector<int> _shape;
     QStringList _str;
};

#endif // MYWIDGET_H
