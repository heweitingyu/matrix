#ifndef MYTREEWIDGET_H
#define MYTREEWIDGET_H
#include<QTreeWidget>
#include<QDrag>
#include<QPoint>
#include<QMouseEvent>
#include<QApplication>
#include<QMimeData>
#include<QPainter>
class mytreewidget : public QTreeWidget
{
public:
    mytreewidget(QWidget *parent);
    ~mytreewidget();
protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void dragEnterEvent(QDragEnterEvent *event);
    void dragMoveEvent(QDragMoveEvent *event);
    void dropEvent(QDropEvent *event);
private:
    //记录拖拽的起点.
    QPoint m_dragPoint;
    //记录被拖拽的项.
    QTreeWidgetItem *m_dragItem;
};

#endif // MYTREEWIDGET_H
